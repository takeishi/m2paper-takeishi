/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Based on 
 *      NS-2 AODV model developed by the CMU/MONARCH group and optimized and
 *      tuned by Samir Das and Mahesh Marina, University of Cincinnati;
 * 
 *      AODV-UU implementation by Erik Nordström of Uppsala University
 *      http://core.it.uu.se/core/index.php/AODV-UU
 *
 * Authors: Elena Buchatskaia <borovkovaes@iitp.ru>
 *          Pavel Boyko <boyko@iitp.ru>
 */
#define NS_LOG_APPEND_CONTEXT                                           \
  if (m_ipv4) { std::clog << "[node " << m_ipv4->GetObject<Node> ()->GetId () << "] "; } 

#include "aodv-routing-protocol.h"
#include "ns3/log.h"
#include "ns3/boolean.h"
#include "ns3/random-variable-stream.h"
#include "ns3/inet-socket-address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/udp-header.h"
#include "ns3/wifi-net-device.h"
#include "ns3/adhoc-wifi-mac.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include <algorithm>
#include <limits>

#include <iostream>
#include <stdio.h>
#include <list>
#include <vector>
#include <cstdlib>
#include <math.h>
unsigned int aodvtimes=0;
unsigned long numSendAodv=0;
int req_list_gd[100][100];

bool flag_check=true;
//経路確認フラグ　0:なし 1:あり
#define T_FLAG 0
int docchi=0;

float add_hop = 1.5;

//地上のノード配置によってかわるID  たて・ななめ 77  よこ:90
#define START_GND 77+153

#define BUF 256

//ノード数
#define NODE 249


//ホップ数全部同じか 0:同じ 1:ペナルティあり(node固定) 2ペナルティあり(CAC) 3ペナルティあり(CAC+time制限)
int normal = 1;

//地上ノードの配置 0:たて 1:よこ 2:ななめ
int ground = 0;

int getMoveID(int recvNodeID){
   int tateID[8]={171,173,21,23,92,107,102,117};
   int yokoID[12]={60,61,62,63,64,65,66,67,68,69,70,71};
   //int nanameID[16]=
   int g_loop;
   if(ground==0){
     g_loop = 18;
     for(int i=0;i<g_loop;i++){
        if(tateID[i] == recvNodeID)
          return 1;
     }
   }else if(ground == 1){
     g_loop = 12;
     for(int i=0;i<g_loop;i++){
        if(yokoID[i] == recvNodeID)
          return 1;
     }
   }
  return -1;
}

double sendBy_time = 30.0;
double reply_time[6]={0.0,0.0,0.0,0.0,0.0,0.0};
int reset_flag=0;

std::list<int> routeID;
std::list<int>::iterator itr;

struct replyRoute{
  double recv_time;
  int recv_nodeID;
  int preID;
  int postID;
  int destID;
  int orgnID;
};

std::vector<replyRoute> reply_route;

//vectorから表示した分を削除する関数
void removeFromVector(int startID,int orgnID,int destID){
  //std::cout<<"vectorから削除します\n";
  int itr=0;
  int nextRemoveID = startID;
  int hopCount = 0;
  int size = (int)reply_route.size();
  //int correntID = startID;

  //std::cout <<"削除前:\n";
 // std::cout << destID << " -> ";
  for(itr=0;itr < size;itr++){
    if(reply_route[itr].recv_nodeID == nextRemoveID && reply_route[itr].orgnID == orgnID)
      nextRemoveID = reply_route[itr].postID;
   // std::cout << reply_route[itr].recv_nodeID << " -> ";
  }
 // std::cout << orgnID << "\n";

  //末尾の目印
  replyRoute push_route={0.0,-1,-1,-1,-1,-1};
  reply_route.push_back(push_route);

  std::cout <<"size:"<< size << " route:" <<destID << " -> ";
  hopCount++;
  itr=0;
  nextRemoveID = startID;
  while(reply_route.size() > 0){
    if(reply_route[itr].recv_time == 0.0){
      break;
    }
    if(reply_route[itr].recv_nodeID == nextRemoveID && reply_route[itr].orgnID == orgnID){
      nextRemoveID = reply_route[itr].postID;
      std::cout << reply_route[itr].recv_nodeID << " -> ";
      reply_route.erase(reply_route.begin() + itr);
      hopCount++;
      continue;
    }
    itr++;
  }
  std::cout << orgnID << "\n";
  reply_route.pop_back();
  std::cout<<"vectorから削除しました "<<reply_route.size()<< " hop="<<hopCount<<"\n";
}

//sendRequestのときにvectorから削除する関数
void removeRestart(int destID){
  int itr=0;
  replyRoute push_route={0.0,-1,-1,-1,-1,-1};
  reply_route.push_back(push_route);
  while(reply_route.size() > 0){
    if(reply_route[itr].recv_time == 0.0){
      break;
    }
    if(reply_route[itr].destID == destID){
      reply_route.erase(reply_route.begin() + itr);
      continue;
    }
    itr++;
  }
  reply_route.pop_back();

}

/*
*
*
*    シナリオファイルで取得した混雑度を読み込む機能を以下に実装
*
*
*
*/

double cong[NODE];

double weight[NODE][6];

int judge_time=1;

void setCongestionParam(){
    FILE *fp;
    char filename[BUF]={'\0'};
    char str[BUF]={'\0'};
/*
puts("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
printf("setCongestionParam %d回目\n",judge_time);
puts("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
*/
    //ネーミングのベース部分
    strcpy(filename,"/home/ns3/ns-3.26/bake/source/ns-3.26/src/aodv/model/congestion.txt");

      
    //ファイルポインタを設定
    fp = fopen(filename,"r");
    if(fp==NULL){
    printf("miss %s\n",filename);
    return;
    }

    int nodeID=0;
    int flowID=0;
    int cong_flag=0;
    while(fgets(str,BUF,fp) != NULL){
      if(strstr(str,"Node")){
        char tmp[100]={};//ファイルから読み込んだ行をコピーするための文字列
        flowID=0;
        cong_flag=0;

        //congestion.txtから読み込んだ行を編集用にコピー
        strcpy(tmp,str);

        char *ptr=NULL;
//Node099 336.00 0.00 0.00 0.00 0.00 0.00 0.00
//0 41.002 r 64
        //コピーした文字列を　スペース　で分割
        ptr=strtok(tmp," ");
        if(ptr!=NULL){
	  // printf("a %s ",ptr);
	   while((ptr=strtok(NULL," "))!=NULL){
	      //printf("%s ",ptr);
              if(cong_flag==0){
                cong_flag=1;
                cong[nodeID] = (double)atoi(ptr);
                //printf("cong %s ",ptr);
              }else{
                weight[nodeID][flowID] = (double)atoi(ptr);
                flowID++;
                //printf(" %s ",ptr);
              }
           }
           //puts("");
        }else{
          puts("miss");
        }
	nodeID++;
      } 
    }


/*
    for(int i=0;i<101;i++){
      char all_weight[BUF]={'\0'};
      char str_cong[BUF]={'\0'};
      sprintf(str_cong, "%.2lf",cong[i]);

      for(int j=0;j<6;j++){
        char str_weight[BUF]={'\0'};
        sprintf(str_weight, "%0.2lf",weight[i][j]);
        strcat(all_weight,str_weight);
        if(j<5)
          strcat(all_weight," ");  
      }
//    fprintf(fp,"nodeID 混雑度 フロー1 フロー2 フロー3 フロー4 フロー5 フロー6",str);
      fprintf(fp,"Node%03d %s %s\n",i,str_cong,all_weight);
    }
*/
    fclose(fp);
    judge_time++;

}


double logn(double base, double antilog){
/*  この形？
*
*   log_b(1/X - 1)
*   0<b<1
*   1が漸近線
*   X=
*
*/
  return log(antilog) / log(base);
}

int dstIndex(int last_num_dst){
  int baseIndex;
  if(last_num_dst<=77+153){
    baseIndex = 72+153;
  }else{
    baseIndex = 84+153;
  }

  if(last_num_dst >= baseIndex && last_num_dst <= baseIndex + 5)
    return last_num_dst - baseIndex;
  else
    return -1;
}

double getCongestion(int recv_ID,int flow_index){
  double  flow_weight=0.0;
  for(int i=0;i<flow_index;i++){
    flow_weight += weight[recv_ID][i];
  }
  return flow_weight;
}


/*
  int beforeID = -1;
  int ini_beforeID = -1;
  for(int loop_print=0;loop_print < route_size;loop_print++){
  if( beforeID == -1 && reply_route[loop_print].preID == -1){
  if(ini_beforeID == -1){
  ini_beforeID = reply_route[loop_print].recv_nodeID;
  }
  beforeID = reply_route[loop_print].recv_nodeID;
  std::cout << "start "<<reply_route[loop_print].recv_nodeID << " from " << reply_route[loop_print].preID <<"\n";
  }else if(beforeID == reply_route[loop_print].preID){
  std::cout << reply_route[loop_print].recv_nodeID << " from " << reply_route[loop_print].preID <<"\n";
  beforeID = reply_route[loop_print].recv_nodeID;
  }
  }
*/

namespace ns3
{

  NS_LOG_COMPONENT_DEFINE ("AodvRoutingProtocol");

  namespace aodv
  {
    NS_OBJECT_ENSURE_REGISTERED (RoutingProtocol);

    /// UDP Port for AODV control traffic
    const uint32_t RoutingProtocol::AODV_PORT = 654;

    //-----------------------------------------------------------------------------
    /// Tag used by AODV implementation

    class DeferredRouteOutputTag : public Tag
    {

    public:
      DeferredRouteOutputTag (int32_t o = -1) : Tag (), m_oif (o) {}

      static TypeId GetTypeId ()
      {
        static TypeId tid = TypeId ("ns3::aodv::DeferredRouteOutputTag")
          .SetParent<Tag> ()
          .SetGroupName("Aodv")
          .AddConstructor<DeferredRouteOutputTag> ()
          ;
        return tid;
      }

      TypeId  GetInstanceTypeId () const 
      {
        return GetTypeId ();
      }

      int32_t GetInterface() const
      {
        return m_oif;
      }

      void SetInterface(int32_t oif)
      {
        m_oif = oif;
      }

      uint32_t GetSerializedSize () const
      {
        return sizeof(int32_t);
      }

      void  Serialize (TagBuffer i) const
      {
        i.WriteU32 (m_oif);
      }

      void  Deserialize (TagBuffer i)
      {
        m_oif = i.ReadU32 ();
      }

      void  Print (std::ostream &os) const
      {
        os << "DeferredRouteOutputTag: output interface = " << m_oif;
      }


    private:
      /// Positive if output device is fixed in RouteOutput
      int32_t m_oif;
    };

    NS_OBJECT_ENSURE_REGISTERED (DeferredRouteOutputTag);


    //-----------------------------------------------------------------------------
    RoutingProtocol::RoutingProtocol () :
      m_rreqRetries (2),
      m_ttlStart (1),
      m_ttlIncrement (10),//default 2
      m_ttlThreshold (150),//default 7  next 100
      m_timeoutBuffer (2),
      m_rreqRateLimit (10),
      m_rerrRateLimit (10),
      m_activeRouteTimeout (Seconds (5)),//defalut 3
      m_netDiameter (35),
      m_nodeTraversalTime (MilliSeconds (40)),
      m_netTraversalTime (Time ((2 * m_netDiameter) * m_nodeTraversalTime)),
      m_pathDiscoveryTime ( Time (2 * m_netTraversalTime)),
      m_myRouteTimeout (Time (2 * std::max (m_pathDiscoveryTime, m_activeRouteTimeout))),
              m_helloInterval (Seconds (3)),//default 1
              m_allowedHelloLoss (2),
              m_deletePeriod (Time (5 * std::max (m_activeRouteTimeout, m_helloInterval))),
              m_nextHopWait (m_nodeTraversalTime + MilliSeconds (10)),
              m_blackListTimeout (Time (m_rreqRetries * m_netTraversalTime)),
              m_maxQueueLen (64),
              m_maxQueueTime (Seconds (30)),
              m_destinationOnly (false),//オリジナルはfalse
              m_gratuitousReply (true),
              m_enableHello (true),
              m_routingTable (m_deletePeriod),
              m_queue (m_maxQueueLen, m_maxQueueTime),
              m_requestId (0),
              m_seqNo (0),
              m_rreqIdCache (m_pathDiscoveryTime),
              m_dpd (m_pathDiscoveryTime),
              m_nb (m_helloInterval),
              m_rreqCount (0),
              m_rerrCount (0),
              m_htimer (Timer::CANCEL_ON_DESTROY),
              m_rreqRateLimitTimer (Timer::CANCEL_ON_DESTROY),
              m_rerrRateLimitTimer (Timer::CANCEL_ON_DESTROY),
              m_lastBcastTime (Seconds (0))
    {
      //std::cout <<"Called: aodv-routing-protocol.cc " << ++aodvtimes << "\n";

      m_nb.SetCallback (MakeCallback (&RoutingProtocol::SendRerrWhenBreaksLinkToNextHop, this));
      //std::cout<<"*";
      int i_d=0;
      req_no=0;
      req_no_d=0;
      for(i_d=0;i_d<100;i_d++){
        req_list[i_d]=-1;
        req_list_d[i_d]=-1;
      }
      if(aodvtimes==0){
        for(int i=0;i<100;i++){
          for(int j=0;j<100;j++){
            req_list_gd[i][j]=-1;
          }
        }
        aodvtimes++;
      }
      d_only_debug=m_destinationOnly;

  for(int i=0;i<NODE;i++){
    cong[i]=0.0;
    for(int j=0;j<6;j++){
      weight[i][j]=0.0;
    }
  }


    }

    TypeId
    RoutingProtocol::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::aodv::RoutingProtocol")
        .SetParent<Ipv4RoutingProtocol> ()
        .SetGroupName("Aodv")
        .AddConstructor<RoutingProtocol> ()
        .AddAttribute ("HelloInterval", "HELLO messages emission interval.",
                       TimeValue (Seconds (1)),
                       MakeTimeAccessor (&RoutingProtocol::m_helloInterval),
                       MakeTimeChecker ())
        .AddAttribute ("TtlStart", "Initial TTL value for RREQ.",
                       UintegerValue (1),
                       MakeUintegerAccessor (&RoutingProtocol::m_ttlStart),
                       MakeUintegerChecker<uint16_t> ())
        .AddAttribute ("TtlIncrement", "TTL increment for each attempt using the expanding ring search for RREQ dissemination.",
                       UintegerValue (2),
                       MakeUintegerAccessor (&RoutingProtocol::m_ttlIncrement),
                       MakeUintegerChecker<uint16_t> ())
        .AddAttribute ("TtlThreshold", "Maximum TTL value for expanding ring search, TTL = NetDiameter is used beyond this value.",
                       UintegerValue (7),
                       MakeUintegerAccessor (&RoutingProtocol::m_ttlThreshold),
                       MakeUintegerChecker<uint16_t> ())
        .AddAttribute ("TimeoutBuffer", "Provide a buffer for the timeout.",
                       UintegerValue (2),
                       MakeUintegerAccessor (&RoutingProtocol::m_timeoutBuffer),
                       MakeUintegerChecker<uint16_t> ())
        .AddAttribute ("RreqRetries", "Maximum number of retransmissions of RREQ to discover a route",
                       UintegerValue (2),
                       MakeUintegerAccessor (&RoutingProtocol::m_rreqRetries),
                       MakeUintegerChecker<uint32_t> ())
        .AddAttribute ("RreqRateLimit", "Maximum number of RREQ per second.",
                       UintegerValue (10),
                       MakeUintegerAccessor (&RoutingProtocol::m_rreqRateLimit),
                       MakeUintegerChecker<uint32_t> ())
        .AddAttribute ("RerrRateLimit", "Maximum number of RERR per second.",
                       UintegerValue (10),
                       MakeUintegerAccessor (&RoutingProtocol::m_rerrRateLimit),
                       MakeUintegerChecker<uint32_t> ())
        .AddAttribute ("NodeTraversalTime", "Conservative estimate of the average one hop traversal time for packets and should include "
                       "queuing delays, interrupt processing times and transfer times.",
                       TimeValue (MilliSeconds (40)),
                       MakeTimeAccessor (&RoutingProtocol::m_nodeTraversalTime),
                       MakeTimeChecker ())
        .AddAttribute ("NextHopWait", "Period of our waiting for the neighbour's RREP_ACK = 10 ms + NodeTraversalTime",
                       TimeValue (MilliSeconds (50)),
                       MakeTimeAccessor (&RoutingProtocol::m_nextHopWait),
                       MakeTimeChecker ())
        .AddAttribute ("ActiveRouteTimeout", "Period of time during which the route is considered to be valid",
                       TimeValue (Seconds (3)),
                       MakeTimeAccessor (&RoutingProtocol::m_activeRouteTimeout),
                       MakeTimeChecker ())
        .AddAttribute ("MyRouteTimeout", "Value of lifetime field in RREP generating by this node = 2 * max(ActiveRouteTimeout, PathDiscoveryTime)",
                       TimeValue (Seconds (11.2)),
                       MakeTimeAccessor (&RoutingProtocol::m_myRouteTimeout),
                       MakeTimeChecker ())
        .AddAttribute ("BlackListTimeout", "Time for which the node is put into the blacklist = RreqRetries * NetTraversalTime",
                       TimeValue (Seconds (5.6)),
                       MakeTimeAccessor (&RoutingProtocol::m_blackListTimeout),
                       MakeTimeChecker ())
        .AddAttribute ("DeletePeriod", "DeletePeriod is intended to provide an upper bound on the time for which an upstream node A "
                       "can have a neighbor B as an active next hop for destination D, while B has invalidated the route to D."
                       " = 5 * max (HelloInterval, ActiveRouteTimeout)",
                       TimeValue (Seconds (15)),
                       MakeTimeAccessor (&RoutingProtocol::m_deletePeriod),
                       MakeTimeChecker ())
        .AddAttribute ("NetDiameter", "Net diameter measures the maximum possible number of hops between two nodes in the network",
                       UintegerValue (35),
                       MakeUintegerAccessor (&RoutingProtocol::m_netDiameter),
                       MakeUintegerChecker<uint32_t> ())
        .AddAttribute ("NetTraversalTime", "Estimate of the average net traversal time = 2 * NodeTraversalTime * NetDiameter",
                       TimeValue (Seconds (2.8)),
                       MakeTimeAccessor (&RoutingProtocol::m_netTraversalTime),
                       MakeTimeChecker ())
        .AddAttribute ("PathDiscoveryTime", "Estimate of maximum time needed to find route in network = 2 * NetTraversalTime",
                       TimeValue (Seconds (5.6)),
                       MakeTimeAccessor (&RoutingProtocol::m_pathDiscoveryTime),
                       MakeTimeChecker ())
        .AddAttribute ("MaxQueueLen", "Maximum number of packets that we allow a routing protocol to buffer.",
                       UintegerValue (64),
                       MakeUintegerAccessor (&RoutingProtocol::SetMaxQueueLen,
                                             &RoutingProtocol::GetMaxQueueLen),
                       MakeUintegerChecker<uint32_t> ())
        .AddAttribute ("MaxQueueTime", "Maximum time packets can be queued (in seconds)",
                       TimeValue (Seconds (30)),
                       MakeTimeAccessor (&RoutingProtocol::SetMaxQueueTime,
                                         &RoutingProtocol::GetMaxQueueTime),
                       MakeTimeChecker ())
        .AddAttribute ("AllowedHelloLoss", "Number of hello messages which may be loss for valid link.",
                       UintegerValue (2),
                       MakeUintegerAccessor (&RoutingProtocol::m_allowedHelloLoss),
                       MakeUintegerChecker<uint16_t> ())
        .AddAttribute ("GratuitousReply", "Indicates whether a gratuitous RREP should be unicast to the node originated route discovery.",
                       BooleanValue (true),
                       MakeBooleanAccessor (&RoutingProtocol::SetGratuitousReplyFlag,
                                            &RoutingProtocol::GetGratuitousReplyFlag),
                       MakeBooleanChecker ())
        .AddAttribute ("DestinationOnly", "Indicates only the destination may respond to this RREQ.",
                       BooleanValue (false),
                       MakeBooleanAccessor (&RoutingProtocol::SetDesinationOnlyFlag,
                                            &RoutingProtocol::GetDesinationOnlyFlag),
                       MakeBooleanChecker ())
        .AddAttribute ("EnableHello", "Indicates whether a hello messages enable.",
                       BooleanValue (true),
                       MakeBooleanAccessor (&RoutingProtocol::SetHelloEnable,
                                            &RoutingProtocol::GetHelloEnable),
                       MakeBooleanChecker ())
        .AddAttribute ("EnableBroadcast", "Indicates whether a broadcast data packets forwarding enable.",
                       BooleanValue (true),
                       MakeBooleanAccessor (&RoutingProtocol::SetBroadcastEnable,
                                            &RoutingProtocol::GetBroadcastEnable),
                       MakeBooleanChecker ())
        .AddAttribute ("UniformRv",
                       "Access to the underlying UniformRandomVariable",
                       StringValue ("ns3::UniformRandomVariable"),
                       MakePointerAccessor (&RoutingProtocol::m_uniformRandomVariable),
                       MakePointerChecker<UniformRandomVariable> ())
        ;
      return tid;
    }

   void scheduleFunc()
   {
     std::cout<<"予約テスト(aodv-routing-protocol.cc) Time:  "<< Simulator::Now().GetSeconds()<<" s\n";
   }
/*
*
*
*   ペナルティ計算
*
*
*
*/

int setPenalty(int recv_ID,int flow_index,int rreq){
  int penalty=0;
  double  flow_weight=0.0;

  int order[6]={0,1,2,3,4,5};
  double order_d[6]={0.0};

  for(int i=0;i<6;i++){
    order_d[i] = weight[recv_ID][i];
  }

  int s=0;
  int t=0;
  double tmp_d=0.0;
  int tmp_i=0;

  for(s=0;s<6;s++){
    for(t=s+1;t<6;t++){
      if(order_d[s] > order_d[t]){
        tmp_d = order_d[s];
        order_d[s] = order_d[t];
        order_d[t] = tmp_d;
        
        tmp_i = order[s];
        order[s] = order[t];
        order[t] = tmp_i;
      }
    }
  }

//  for(int i=0;i<flow_index;i++){
//    flow_weight += weight[recv_ID][i];
//  }

  for(int i=0;i<6;i++){
    if(flow_index == order[i])
      break;
    flow_weight +=  order_d[i];
  }

  double total_weight=0.0;
  for(int i=0;i<6;i++){
    total_weight += weight[recv_ID][i];
  }
//byte -> bit 
  flow_weight = flow_weight * 8;
  total_weight = total_weight * 8;



if(weight[recv_ID][flow_index]*8>=(300000)){
        penalty += 2;//2
}else if(weight[recv_ID][flow_index]*8>=150000){
        penalty += 1;//1
}else{
        penalty += 0;
}
/*
  if(flow_weight >= 2000000){// * 2   (double)(1024*1024)*3.5
        penalty += 8;//penalty = 10
        if(rreq == 2){
          std::cout<<"----------------------------\n";
          std::cout<<"Node " << recv_ID << " reached Limit. FlowID: "<< flow_index <<"\n";
          std::cout<< "Time: " << Simulator::Now().GetSeconds() << "\n";
          printf("flow_weight: %.0lf\n",flow_weight);
          printf("total_weight: %.0lf\n",total_weight);
          std::cout<<"----------------------------\n";
        }
        //return -1;

  }else if(flow_weight >= 1000000){
        penalty += 3;// 3
  }else{
        penalty+=0;
  }
*/




  if(flow_weight >= (double)(1024*1024)*2){// * 2
        penalty += 2;//penalty = 10
        if(rreq == 2){
          std::cout<<"----------------------------\n";
          std::cout<<"Node " << recv_ID << " reached Limit. FlowID: "<< flow_index <<"\n";
          std::cout<< "Time: " << Simulator::Now().GetSeconds() << "\n";
          printf("flow_weight: %.0lf\n",flow_weight);
          printf("total_weight: %.0lf\n",total_weight);
          std::cout<<"----------------------------\n";
        }


  }else if(flow_weight >= 1000000){
        penalty += 1;// 3 origin:2
  }else{
        penalty+=0;
  }

/*
  if(total_weight >= (double)(1024*1024)*3.5){// * 2
        penalty += 8;//penalty = 10
        if(rreq == 2){
          std::cout<<"----------------------------\n";
          std::cout<<"Node " << recv_ID << " reached Limit. FlowID: "<< flow_index <<"\n";
          std::cout<< "Time: " << Simulator::Now().GetSeconds() << "\n";
          printf("flow_weight: %.0lf\n",flow_weight);
          printf("total_weight: %.0lf\n",total_weight);
          std::cout<<"----------------------------\n";
        }


  }else if(total_weight >= 1000000){
        penalty += 2;// 3 origin:2
  }else{
        penalty+=0;
  }
*/


  return penalty;
}


    void
    RoutingProtocol::SetMaxQueueLen (uint32_t len)
    {
      m_maxQueueLen = len;
      m_queue.SetMaxQueueLen (len);
    }
    void
    RoutingProtocol::SetMaxQueueTime (Time t)
    {
      m_maxQueueTime = t;
      m_queue.SetQueueTimeout (t);
    }

    RoutingProtocol::~RoutingProtocol ()
    {
    }

    void
    RoutingProtocol::DoDispose ()
    {
      m_ipv4 = 0;
      for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::iterator iter =
             m_socketAddresses.begin (); iter != m_socketAddresses.end (); iter++)
        {
          iter->first->Close ();
        }
      m_socketAddresses.clear ();
      for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::iterator iter =
             m_socketSubnetBroadcastAddresses.begin (); iter != m_socketSubnetBroadcastAddresses.end (); iter++)
        {
          iter->first->Close ();
        }
      m_socketSubnetBroadcastAddresses.clear ();
      Ipv4RoutingProtocol::DoDispose ();
    }

    void
    RoutingProtocol::PrintRoutingTable (Ptr<OutputStreamWrapper> stream) const
    {
      *stream->GetStream () << "Node: " << m_ipv4->GetObject<Node> ()->GetId ()
                            << "; Time: " << Now().As (Time::S)
                            << ", Local time: " << GetObject<Node> ()->GetLocalTime ().As (Time::S)
                            << ", AODV Routing table" << std::endl;

      m_routingTable.Print (stream);
      *stream->GetStream () << std::endl;
    }

    int64_t
    RoutingProtocol::AssignStreams (int64_t stream)
    {
      NS_LOG_FUNCTION (this << stream);
      m_uniformRandomVariable->SetStream (stream);
      return 1;
    }

    void
    RoutingProtocol::Start ()
    {
      NS_LOG_FUNCTION (this);
      if (m_enableHello)
        {
          m_nb.ScheduleTimer ();
        }
      m_rreqRateLimitTimer.SetFunction (&RoutingProtocol::RreqRateLimitTimerExpire,
                                        this);
      m_rreqRateLimitTimer.Schedule (Seconds (1));

      m_rerrRateLimitTimer.SetFunction (&RoutingProtocol::RerrRateLimitTimerExpire,
                                        this);
      m_rerrRateLimitTimer.Schedule (Seconds (1));

    }

    Ptr<Ipv4Route>
    RoutingProtocol::RouteOutput (Ptr<Packet> p, const Ipv4Header &header,
                                  Ptr<NetDevice> oif, Socket::SocketErrno &sockerr)
    {
      NS_LOG_FUNCTION (this << header << (oif ? oif->GetIfIndex () : 0));
      if (!p)
        {
          NS_LOG_DEBUG("Packet is == 0");
          return LoopbackRoute (header, oif); // later
        }
      if (m_socketAddresses.empty ())
        {
          sockerr = Socket::ERROR_NOROUTETOHOST;
          NS_LOG_LOGIC ("No aodv interfaces");
          Ptr<Ipv4Route> route;
          return route;
        }
      sockerr = Socket::ERROR_NOTERROR;
      Ptr<Ipv4Route> route;
      Ipv4Address dst = header.GetDestination ();
      RoutingTableEntry rt;
      if (m_routingTable.LookupValidRoute (dst, rt))
        {
          route = rt.GetRoute ();
          NS_ASSERT (route != 0);
          NS_LOG_DEBUG ("Exist route to " << route->GetDestination () << " from interface " << route->GetSource ());
          if (oif != 0 && route->GetOutputDevice () != oif)
            {
              NS_LOG_DEBUG ("Output device doesn't match. Dropped.");
              sockerr = Socket::ERROR_NOROUTETOHOST;
              return Ptr<Ipv4Route> ();
            }
          UpdateRouteLifeTime (dst, m_activeRouteTimeout);
          UpdateRouteLifeTime (route->GetGateway (), m_activeRouteTimeout);
          return route;
        }

      // Valid route not found, in this case we return loopback. 
      // Actual route request will be deferred until packet will be fully formed, 
      // routed to loopback, received from loopback and passed to RouteInput (see below)
      uint32_t iif = (oif ? m_ipv4->GetInterfaceForDevice (oif) : -1);
      DeferredRouteOutputTag tag (iif);
      NS_LOG_DEBUG ("Valid Route not found");
      if (!p->PeekPacketTag (tag))
        {
          p->AddPacketTag (tag);
        }
      return LoopbackRoute (header, oif);
    }

    void
    RoutingProtocol::DeferredRouteOutput (Ptr<const Packet> p, const Ipv4Header & header, 
                                          UnicastForwardCallback ucb, ErrorCallback ecb)
    {
      NS_LOG_FUNCTION (this << p << header);
      NS_ASSERT (p != 0 && p != Ptr<Packet> ());

      QueueEntry newEntry (p, header, ucb, ecb);
      bool result = m_queue.Enqueue (newEntry);
      if (result)
        {
          NS_LOG_LOGIC ("Add packet " << p->GetUid () << " to queue. Protocol " << (uint16_t) header.GetProtocol ());
          RoutingTableEntry rt;
          bool result = m_routingTable.LookupRoute (header.GetDestination (), rt);
          if(!result || ((rt.GetFlag () != IN_SEARCH) && result))
            {
              NS_LOG_LOGIC ("Send new RREQ for outbound packet to " <<header.GetDestination ());
              SendRequest (header.GetDestination ());
            }
        }
    }

    bool
    RoutingProtocol::RouteInput (Ptr<const Packet> p, const Ipv4Header &header,
                                 Ptr<const NetDevice> idev, UnicastForwardCallback ucb,
                                 MulticastForwardCallback mcb, LocalDeliverCallback lcb, ErrorCallback ecb)
    {
      NS_LOG_FUNCTION (this << p->GetUid () << header.GetDestination () << idev->GetAddress ());
      if (m_socketAddresses.empty ())
        {
          NS_LOG_LOGIC ("No aodv interfaces");
          return false;
        }
      NS_ASSERT (m_ipv4 != 0);
      NS_ASSERT (p != 0);
      // Check if input device supports IP
      NS_ASSERT (m_ipv4->GetInterfaceForDevice (idev) >= 0);
      int32_t iif = m_ipv4->GetInterfaceForDevice (idev);

      Ipv4Address dst = header.GetDestination ();
      Ipv4Address origin = header.GetSource ();

      // Deferred route request
      if (idev == m_lo)
        {
          DeferredRouteOutputTag tag;
          if (p->PeekPacketTag (tag))
            {
              DeferredRouteOutput (p, header, ucb, ecb);
              return true;
            }
        }

      // Duplicate of own packet
      if (IsMyOwnAddress (origin))
        return true;

      // AODV is not a multicast routing protocol
      if (dst.IsMulticast ())
        {
          return false; 
        }

      // Broadcast local delivery/forwarding
      for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j =
             m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
        {
          Ipv4InterfaceAddress iface = j->second;
          if (m_ipv4->GetInterfaceForAddress (iface.GetLocal ()) == iif)
            if (dst == iface.GetBroadcast () || dst.IsBroadcast ())
              {
                if (m_dpd.IsDuplicate (p, header))
                  {
                    NS_LOG_DEBUG ("Duplicated packet " << p->GetUid () << " from " << origin << ". Drop.");
                    return true;
                  }
                UpdateRouteLifeTime (origin, m_activeRouteTimeout);
                Ptr<Packet> packet = p->Copy ();
                if (lcb.IsNull () == false)
                  {
                    NS_LOG_LOGIC ("Broadcast local delivery to " << iface.GetLocal ());
                    lcb (p, header, iif);
                    // Fall through to additional processing
                  }
                else
                  {
                    NS_LOG_ERROR ("Unable to deliver packet locally due to null callback " << p->GetUid () << " from " << origin);
                    ecb (p, header, Socket::ERROR_NOROUTETOHOST);
                  }
                if (!m_enableBroadcast)
                  {
                    return true;
                  }
                if (header.GetProtocol () == UdpL4Protocol::PROT_NUMBER)
                  {
                    UdpHeader udpHeader;
                    p->PeekHeader (udpHeader);
                    if (udpHeader.GetDestinationPort () == AODV_PORT)
                      {
                        // AODV packets sent in broadcast are already managed
                        return true;
                      }
                  }
                if (header.GetTtl () > 1)
                  {
                    NS_LOG_LOGIC ("Forward broadcast. TTL " << (uint16_t) header.GetTtl ());
                    RoutingTableEntry toBroadcast;
                    if (m_routingTable.LookupRoute (dst, toBroadcast))
                      {
                        Ptr<Ipv4Route> route = toBroadcast.GetRoute ();
                        ucb (route, packet, header);
                      }
                    else
                      {
                        NS_LOG_DEBUG ("No route to forward broadcast. Drop packet " << p->GetUid ());
                      }
                  }
                else
                  {
                    NS_LOG_DEBUG ("TTL exceeded. Drop packet " << p->GetUid ());
                  }
                return true;
              }
        }

      // Unicast local delivery
      if (m_ipv4->IsDestinationAddress (dst, iif))
        {
          UpdateRouteLifeTime (origin, m_activeRouteTimeout);
          RoutingTableEntry toOrigin;
          if (m_routingTable.LookupValidRoute (origin, toOrigin))
            {
              UpdateRouteLifeTime (toOrigin.GetNextHop (), m_activeRouteTimeout);
              m_nb.Update (toOrigin.GetNextHop (), m_activeRouteTimeout);
            }
          if (lcb.IsNull () == false)
            {
              NS_LOG_LOGIC ("Unicast local delivery to " << dst);
              lcb (p, header, iif);
            }
          else
            {
              NS_LOG_ERROR ("Unable to deliver packet locally due to null callback " << p->GetUid () << " from " << origin);
              ecb (p, header, Socket::ERROR_NOROUTETOHOST);
            }
          return true;
        }

      // Check if input device supports IP forwarding
      if (m_ipv4->IsForwarding (iif) == false)
        {
          NS_LOG_LOGIC ("Forwarding disabled for this interface");
          ecb (p, header, Socket::ERROR_NOROUTETOHOST);
          return true;
        }

      // Forwarding
      return Forwarding (p, header, ucb, ecb);
    }

    bool
    RoutingProtocol::Forwarding (Ptr<const Packet> p, const Ipv4Header & header,
                                 UnicastForwardCallback ucb, ErrorCallback ecb)
    {
      NS_LOG_FUNCTION (this);
      Ipv4Address dst = header.GetDestination ();
      Ipv4Address origin = header.GetSource ();
      m_routingTable.Purge ();
      RoutingTableEntry toDst;
      if (m_routingTable.LookupRoute (dst, toDst))
        {
          if (toDst.GetFlag () == VALID)
            {
              Ptr<Ipv4Route> route = toDst.GetRoute ();
              NS_LOG_LOGIC (route->GetSource ()<<" forwarding to " << dst << " from " << origin << " packet " << p->GetUid ());

              /*
               *  Each time a route is used to forward a data packet, its Active Route
               *  Lifetime field of the source, destination and the next hop on the
               *  path to the destination is updated to be no less than the current
               *  time plus ActiveRouteTimeout.
               */
              UpdateRouteLifeTime (origin, m_activeRouteTimeout);
              UpdateRouteLifeTime (dst, m_activeRouteTimeout);
              UpdateRouteLifeTime (route->GetGateway (), m_activeRouteTimeout);
              /*
               *  Since the route between each originator and destination pair is expected to be symmetric, the
               *  Active Route Lifetime for the previous hop, along the reverse path back to the IP source, is also updated
               *  to be no less than the current time plus ActiveRouteTimeout
               */
              RoutingTableEntry toOrigin;
              m_routingTable.LookupRoute (origin, toOrigin);
              UpdateRouteLifeTime (toOrigin.GetNextHop (), m_activeRouteTimeout);

              m_nb.Update (route->GetGateway (), m_activeRouteTimeout);
              m_nb.Update (toOrigin.GetNextHop (), m_activeRouteTimeout);

              ucb (route, p, header);
              return true;
            }
          else
            {
              if (toDst.GetValidSeqNo ())
                {
                  SendRerrWhenNoRouteToForward (dst, toDst.GetSeqNo (), origin);
                  NS_LOG_DEBUG ("Drop packet " << p->GetUid () << " because no route to forward it.");
                  return false;
                }
            }
        }
      NS_LOG_LOGIC ("route not found to "<< dst << ". Send RERR message.");
      NS_LOG_DEBUG ("Drop packet " << p->GetUid () << " because no route to forward it.");
      SendRerrWhenNoRouteToForward (dst, 0, origin);
      return false;
    }

    void
    RoutingProtocol::SetIpv4 (Ptr<Ipv4> ipv4)
    {
      NS_ASSERT (ipv4 != 0);
      NS_ASSERT (m_ipv4 == 0);

      m_ipv4 = ipv4;

      // Create lo route. It is asserted that the only one interface up for now is loopback
      NS_ASSERT (m_ipv4->GetNInterfaces () == 1 && m_ipv4->GetAddress (0, 0).GetLocal () == Ipv4Address ("127.0.0.1"));
      m_lo = m_ipv4->GetNetDevice (0);
      NS_ASSERT (m_lo != 0);
      // Remember lo route
      RoutingTableEntry rt (/*device=*/ m_lo, /*dst=*/ Ipv4Address::GetLoopback (), /*know seqno=*/ true, /*seqno=*/ 0,
                            /*iface=*/ Ipv4InterfaceAddress (Ipv4Address::GetLoopback (), Ipv4Mask ("255.0.0.0")),
                            /*hops=*/ 1, /*next hop=*/ Ipv4Address::GetLoopback (),
                            /*lifetime=*/ Simulator::GetMaximumSimulationTime ());
      m_routingTable.AddRoute (rt);

      Simulator::ScheduleNow (&RoutingProtocol::Start, this);
    }

    void
    RoutingProtocol::NotifyInterfaceUp (uint32_t i)
    {
      NS_LOG_FUNCTION (this << m_ipv4->GetAddress (i, 0).GetLocal ());
      Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
      if (l3->GetNAddresses (i) > 1)
        {
          NS_LOG_WARN ("AODV does not work with more then one address per each interface.");
        }
      Ipv4InterfaceAddress iface = l3->GetAddress (i, 0);
      if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
        return;
 
      // Create a socket to listen only on this interface
      Ptr<Socket> socket = Socket::CreateSocket (GetObject<Node> (),
                                                 UdpSocketFactory::GetTypeId ());
      NS_ASSERT (socket != 0);
      socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvAodv, this));
      socket->Bind (InetSocketAddress (iface.GetLocal (), AODV_PORT));
      socket->BindToNetDevice (l3->GetNetDevice (i));
      socket->SetAllowBroadcast (true);
      socket->SetIpRecvTtl (true);
      m_socketAddresses.insert (std::make_pair (socket, iface));

      // create also a subnet broadcast socket
      socket = Socket::CreateSocket (GetObject<Node> (),
                                     UdpSocketFactory::GetTypeId ());
      NS_ASSERT (socket != 0);
      socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvAodv, this));
      socket->Bind (InetSocketAddress (iface.GetBroadcast (), AODV_PORT));
      socket->BindToNetDevice (l3->GetNetDevice (i));
      socket->SetAllowBroadcast (true);
      socket->SetIpRecvTtl (true);
      m_socketSubnetBroadcastAddresses.insert (std::make_pair (socket, iface));

      // Add local broadcast record to the routing table
      Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (iface.GetLocal ()));
      RoutingTableEntry rt (/*device=*/ dev, /*dst=*/ iface.GetBroadcast (), /*know seqno=*/ true, /*seqno=*/ 0, /*iface=*/ iface,
                            /*hops=*/ 1, /*next hop=*/ iface.GetBroadcast (), /*lifetime=*/ Simulator::GetMaximumSimulationTime ());
      m_routingTable.AddRoute (rt);

      if (l3->GetInterface (i)->GetArpCache ())
        {
          m_nb.AddArpCache (l3->GetInterface (i)->GetArpCache ());
        }

      // Allow neighbor manager use this interface for layer 2 feedback if possible
      Ptr<WifiNetDevice> wifi = dev->GetObject<WifiNetDevice> ();
      if (wifi == 0)
        return;
      Ptr<WifiMac> mac = wifi->GetMac ();
      if (mac == 0)
        return;

      mac->TraceConnectWithoutContext ("TxErrHeader", m_nb.GetTxErrorCallback ());
    }

    void
    RoutingProtocol::NotifyInterfaceDown (uint32_t i)
    {
      NS_LOG_FUNCTION (this << m_ipv4->GetAddress (i, 0).GetLocal ());

      // Disable layer 2 link state monitoring (if possible)
      Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
      Ptr<NetDevice> dev = l3->GetNetDevice (i);
      Ptr<WifiNetDevice> wifi = dev->GetObject<WifiNetDevice> ();
      if (wifi != 0)
        {
          Ptr<WifiMac> mac = wifi->GetMac ()->GetObject<AdhocWifiMac> ();
          if (mac != 0)
            {
              mac->TraceDisconnectWithoutContext ("TxErrHeader",
                                                  m_nb.GetTxErrorCallback ());
              m_nb.DelArpCache (l3->GetInterface (i)->GetArpCache ());
            }
        }

      // Close socket 
      Ptr<Socket> socket = FindSocketWithInterfaceAddress (m_ipv4->GetAddress (i, 0));
      NS_ASSERT (socket);
      socket->Close ();
      m_socketAddresses.erase (socket);

      // Close socket
      socket = FindSubnetBroadcastSocketWithInterfaceAddress (m_ipv4->GetAddress (i, 0));
      NS_ASSERT (socket);
      socket->Close ();
      m_socketSubnetBroadcastAddresses.erase (socket);

      if (m_socketAddresses.empty ())
        {
          NS_LOG_LOGIC ("No aodv interfaces");
          m_htimer.Cancel ();
          m_nb.Clear ();
          m_routingTable.Clear ();
          return;
        }
      m_routingTable.DeleteAllRoutesFromInterface (m_ipv4->GetAddress (i, 0));
    }

    void
    RoutingProtocol::NotifyAddAddress (uint32_t i, Ipv4InterfaceAddress address)
    {
      NS_LOG_FUNCTION (this << " interface " << i << " address " << address);
      Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
      if (!l3->IsUp (i))
        return;
      if (l3->GetNAddresses (i) == 1)
        {
          Ipv4InterfaceAddress iface = l3->GetAddress (i, 0);
          Ptr<Socket> socket = FindSocketWithInterfaceAddress (iface);
          if (!socket)
            {
              if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
                return;
              // Create a socket to listen only on this interface
              Ptr<Socket> socket = Socket::CreateSocket (GetObject<Node> (),
                                                         UdpSocketFactory::GetTypeId ());
              NS_ASSERT (socket != 0);
              socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvAodv,this));
              socket->Bind (InetSocketAddress (iface.GetLocal (), AODV_PORT));
              socket->BindToNetDevice (l3->GetNetDevice (i));
              socket->SetAllowBroadcast (true);
              m_socketAddresses.insert (std::make_pair (socket, iface));

              // create also a subnet directed broadcast socket
              socket = Socket::CreateSocket (GetObject<Node> (),
                                             UdpSocketFactory::GetTypeId ());
              NS_ASSERT (socket != 0);
              socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvAodv, this));
              socket->Bind (InetSocketAddress (iface.GetBroadcast (), AODV_PORT));
              socket->BindToNetDevice (l3->GetNetDevice (i));
              socket->SetAllowBroadcast (true);
              socket->SetIpRecvTtl (true);
              m_socketSubnetBroadcastAddresses.insert (std::make_pair (socket, iface));

              // Add local broadcast record to the routing table
              Ptr<NetDevice> dev = m_ipv4->GetNetDevice (
                                                         m_ipv4->GetInterfaceForAddress (iface.GetLocal ()));
              RoutingTableEntry rt (/*device=*/ dev, /*dst=*/ iface.GetBroadcast (), /*know seqno=*/ true,
                                    /*seqno=*/ 0, /*iface=*/ iface, /*hops=*/ 1,
                                    /*next hop=*/ iface.GetBroadcast (), /*lifetime=*/ Simulator::GetMaximumSimulationTime ());
              m_routingTable.AddRoute (rt);
            }
        }
      else
        {
          NS_LOG_LOGIC ("AODV does not work with more then one address per each interface. Ignore added address");
        }
    }

    void
    RoutingProtocol::NotifyRemoveAddress (uint32_t i, Ipv4InterfaceAddress address)
    {
      NS_LOG_FUNCTION (this);
      Ptr<Socket> socket = FindSocketWithInterfaceAddress (address);
      if (socket)
        {
          m_routingTable.DeleteAllRoutesFromInterface (address);
          socket->Close ();
          m_socketAddresses.erase (socket);

          Ptr<Socket> unicastSocket = FindSubnetBroadcastSocketWithInterfaceAddress (address);
          if (unicastSocket)
            {
              unicastSocket->Close ();
              m_socketAddresses.erase (unicastSocket);
            }

          Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
          if (l3->GetNAddresses (i))
            {
              Ipv4InterfaceAddress iface = l3->GetAddress (i, 0);
              // Create a socket to listen only on this interface
              Ptr<Socket> socket = Socket::CreateSocket (GetObject<Node> (),
                                                         UdpSocketFactory::GetTypeId ());
              NS_ASSERT (socket != 0);
              socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvAodv, this));
              // Bind to any IP address so that broadcasts can be received
              socket->Bind (InetSocketAddress (iface.GetLocal (), AODV_PORT));
              socket->BindToNetDevice (l3->GetNetDevice (i));
              socket->SetAllowBroadcast (true);
              socket->SetIpRecvTtl (true);
              m_socketAddresses.insert (std::make_pair (socket, iface));

              // create also a unicast socket
              socket = Socket::CreateSocket (GetObject<Node> (),
                                             UdpSocketFactory::GetTypeId ());
              NS_ASSERT (socket != 0);
              socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvAodv, this));
              socket->Bind (InetSocketAddress (iface.GetBroadcast (), AODV_PORT));
              socket->BindToNetDevice (l3->GetNetDevice (i));
              socket->SetAllowBroadcast (true);
              socket->SetIpRecvTtl (true);
              m_socketSubnetBroadcastAddresses.insert (std::make_pair (socket, iface));

              // Add local broadcast record to the routing table
              Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (iface.GetLocal ()));
              RoutingTableEntry rt (/*device=*/ dev, /*dst=*/ iface.GetBroadcast (), /*know seqno=*/ true, /*seqno=*/ 0, /*iface=*/ iface,
                                    /*hops=*/ 1, /*next hop=*/ iface.GetBroadcast (), /*lifetime=*/ Simulator::GetMaximumSimulationTime ());
              m_routingTable.AddRoute (rt);
            }
          if (m_socketAddresses.empty ())
            {
              NS_LOG_LOGIC ("No aodv interfaces");
              m_htimer.Cancel ();
              m_nb.Clear ();
              m_routingTable.Clear ();
              return;
            }
        }
      else
        {
          NS_LOG_LOGIC ("Remove address not participating in AODV operation");
        }
    }

    bool
    RoutingProtocol::IsMyOwnAddress (Ipv4Address src)
    {
      NS_LOG_FUNCTION (this << src);
      for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j =
             m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
        {
          Ipv4InterfaceAddress iface = j->second;
          if (src == iface.GetLocal ())
            {
              return true;
            }
        }
      return false;
    }

    Ptr<Ipv4Route> 
    RoutingProtocol::LoopbackRoute (const Ipv4Header & hdr, Ptr<NetDevice> oif) const
    {
      NS_LOG_FUNCTION (this << hdr);
      NS_ASSERT (m_lo != 0);
      Ptr<Ipv4Route> rt = Create<Ipv4Route> ();
      rt->SetDestination (hdr.GetDestination ());
      //
      // Source address selection here is tricky.  The loopback route is
      // returned when AODV does not have a route; this causes the packet
      // to be looped back and handled (cached) in RouteInput() method
      // while a route is found. However, connection-oriented protocols
      // like TCP need to create an endpoint four-tuple (src, src port,
      // dst, dst port) and create a pseudo-header for checksumming.  So,
      // AODV needs to guess correctly what the eventual source address
      // will be.
      //
      // For single interface, single address nodes, this is not a problem.
      // When there are possibly multiple outgoing interfaces, the policy
      // implemented here is to pick the first available AODV interface.
      // If RouteOutput() caller specified an outgoing interface, that 
      // further constrains the selection of source address
      //
      std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin ();
      if (oif)
        {
          // Iterate to find an address on the oif device
          for (j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
            {
              Ipv4Address addr = j->second.GetLocal ();
              int32_t interface = m_ipv4->GetInterfaceForAddress (addr);
              if (oif == m_ipv4->GetNetDevice (static_cast<uint32_t> (interface)))
                {
                  rt->SetSource (addr);
                  break;
                }
            }
        }
      else
        {
          rt->SetSource (j->second.GetLocal ());
        }
      NS_ASSERT_MSG (rt->GetSource () != Ipv4Address (), "Valid AODV source address not found");
      rt->SetGateway (Ipv4Address ("127.0.0.1"));
      rt->SetOutputDevice (m_lo);
      return rt;
    }

    void
    RoutingProtocol::SendRequest (Ipv4Address dst)
    {
      //確認

        uint32_t m_addr_dst = dst.Get();
        int last_num_dst = (m_addr_dst >> 0) & 0xff;

        //std::cout<<"   RREQを送ります。dst:"<<last_num_dst - 1<<" time:"<<Simulator::Now().GetSeconds()<<"\n";
      
      //確認ここまで

      NS_LOG_FUNCTION ( this << dst);
      // A node SHOULD NOT originate more than RREQ_RATELIMIT RREQ messages per second.
      if (m_rreqCount == m_rreqRateLimit)
        {
          Simulator::Schedule (m_rreqRateLimitTimer.GetDelayLeft () + MicroSeconds (100),
                               &RoutingProtocol::SendRequest, this, dst);
          return;
        }
      else
        m_rreqCount++;
      // Create RREQ header
      RreqHeader rreqHeader;
      rreqHeader.SetDst (dst);

      RoutingTableEntry rt;
      // Using the Hop field in Routing Table to manage the expanding ring search
      uint16_t ttl = m_ttlStart;
      if (m_routingTable.LookupRoute (dst, rt))
        {
          if (rt.GetFlag () != IN_SEARCH)
            {
              ttl = std::min<uint16_t> (rt.GetHop () + m_ttlIncrement, m_netDiameter);
            }
          else
            {
              ttl = rt.GetHop () + m_ttlIncrement;
              if (ttl > m_ttlThreshold)
                ttl = m_netDiameter;
            }
          if (ttl == m_netDiameter)
            rt.IncrementRreqCnt ();
          if (rt.GetValidSeqNo ())
            rreqHeader.SetDstSeqno (rt.GetSeqNo ());
          else
            rreqHeader.SetUnknownSeqno (true);
          rt.SetHop (ttl);
          rt.SetFlag (IN_SEARCH);
          rt.SetLifeTime (m_pathDiscoveryTime);
          m_routingTable.Update (rt);
        }
      else
        {
          rreqHeader.SetUnknownSeqno (true);
          Ptr<NetDevice> dev = 0;
          RoutingTableEntry newEntry (/*device=*/ dev, /*dst=*/ dst, /*validSeqNo=*/ false, /*seqno=*/ 0,
                                      /*iface=*/ Ipv4InterfaceAddress (),/*hop=*/ ttl,
                                      /*nextHop=*/ Ipv4Address (), /*lifeTime=*/ m_pathDiscoveryTime);
          // Check if TtlStart == NetDiameter
          if (ttl == m_netDiameter)
            newEntry.IncrementRreqCnt ();
          newEntry.SetFlag (IN_SEARCH);
          m_routingTable.AddRoute (newEntry);
        }

      if (m_gratuitousReply)
        rreqHeader.SetGratiousRrep (true);
      if (m_destinationOnly)
        rreqHeader.SetDestinationOnly (true);

      m_seqNo++;
      rreqHeader.SetOriginSeqno (m_seqNo);
      m_requestId++;
      rreqHeader.SetId (m_requestId);

      // Send RREQ as subnet directed broadcast from each interface used by aodv
      for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j =
             m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
        {
          Ptr<Socket> socket = j->first;
          Ipv4InterfaceAddress iface = j->second;

          rreqHeader.SetOrigin (iface.GetLocal ());
          m_rreqIdCache.IsDuplicate (iface.GetLocal (), m_requestId);

          Ptr<Packet> packet = Create<Packet> ();
          SocketIpTtlTag tag;
          tag.SetTtl (ttl);
          packet->AddPacketTag (tag);
          packet->AddHeader (rreqHeader);
          TypeHeader tHeader (AODVTYPE_RREQ);
          packet->AddHeader (tHeader);
          // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
          Ipv4Address destination;
          if (iface.GetMask () == Ipv4Mask::GetOnes ())
            {
              destination = Ipv4Address ("255.255.255.255");
            }
          else
            { 
              destination = iface.GetBroadcast ();
            }
          NS_LOG_DEBUG ("Send RREQ with id " << rreqHeader.GetId () << " to socket");
          m_lastBcastTime = Simulator::Now ();
          Simulator::Schedule (Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0, 10))), &RoutingProtocol::SendTo, this, socket, packet, destination); 
        }
      ScheduleRreqRetry (dst);
      //reply_route.clear();
      removeRestart(last_num_dst - 1);
    }

    void
    RoutingProtocol::SendTo (Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination)
    {
      //std::cout << "AODVパケット送信 "<< ++numSendAodv << "個目\n";
      socket->SendTo (packet, 0, InetSocketAddress (destination, AODV_PORT));

    }
    void
    RoutingProtocol::ScheduleRreqRetry (Ipv4Address dst)
    {
      NS_LOG_FUNCTION (this << dst);
      if (m_addressReqTimer.find (dst) == m_addressReqTimer.end ())
        {
          Timer timer (Timer::CANCEL_ON_DESTROY);
          m_addressReqTimer[dst] = timer;
        }
      m_addressReqTimer[dst].SetFunction (&RoutingProtocol::RouteRequestTimerExpire, this);
      m_addressReqTimer[dst].Remove ();
      m_addressReqTimer[dst].SetArguments (dst);
      RoutingTableEntry rt;
      m_routingTable.LookupRoute (dst, rt);
      Time retry;
      if (rt.GetHop () < m_netDiameter)
        {
          retry = 2 * m_nodeTraversalTime * (rt.GetHop () + m_timeoutBuffer);
        }
      else
        {
          // Binary exponential backoff
          retry = std::pow<uint16_t> (2, rt.GetRreqCnt () - 1) * m_netTraversalTime;
        }
      m_addressReqTimer[dst].Schedule (retry);
      NS_LOG_LOGIC ("Scheduled RREQ retry in " << retry.GetSeconds () << " seconds");
    }

    void
    RoutingProtocol::RecvAodv (Ptr<Socket> socket)
    {
      NS_LOG_FUNCTION (this << socket);
      Address sourceAddress;
      Ptr<Packet> packet = socket->RecvFrom (sourceAddress);
      InetSocketAddress inetSourceAddr = InetSocketAddress::ConvertFrom (sourceAddress);
      Ipv4Address sender = inetSourceAddr.GetIpv4 ();
      Ipv4Address receiver;

      if (m_socketAddresses.find (socket) != m_socketAddresses.end ())
        {
          receiver = m_socketAddresses[socket].GetLocal ();
        }
      else if(m_socketSubnetBroadcastAddresses.find (socket) != m_socketSubnetBroadcastAddresses.end ())
        {
          receiver = m_socketSubnetBroadcastAddresses[socket].GetLocal ();
        }
      else
        {
          NS_ASSERT_MSG (false, "Received a packet from an unknown socket");
        }
      NS_LOG_DEBUG ("AODV node " << this << " received a AODV packet from " << sender << " to " << receiver);

      UpdateRouteToNeighbor (sender, receiver);
      TypeHeader tHeader (AODVTYPE_RREQ);
      packet->RemoveHeader (tHeader);
      if (!tHeader.IsValid ())
        {
          NS_LOG_DEBUG ("AODV message " << packet->GetUid () << " with unknown type received: " << tHeader.Get () << ". Drop");
          return; // drop
        }
      switch (tHeader.Get ())
        {
        case AODVTYPE_RREQ:
          {
            RecvRequest (packet, receiver, sender);
            break;
          }
        case AODVTYPE_RREP:
          {
            RecvReply (packet, receiver, sender);
            break;
          }
        case AODVTYPE_RERR:
          {
            RecvError (packet, sender);
            break;
          }
        case AODVTYPE_RREP_ACK:
          {
            RecvReplyAck (sender);
            break;
          }
        }
    }

    bool
    RoutingProtocol::UpdateRouteLifeTime (Ipv4Address addr, Time lifetime)
    {
      NS_LOG_FUNCTION (this << addr << lifetime);
      RoutingTableEntry rt;
      if (m_routingTable.LookupRoute (addr, rt))
        {
          if (rt.GetFlag () == VALID)
            {
              NS_LOG_DEBUG ("Updating VALID route");
              rt.SetRreqCnt (0);
              rt.SetLifeTime (std::max (lifetime, rt.GetLifeTime ()));
              m_routingTable.Update (rt);
              return true;
            }
        }
      return false;
    }

    void
    RoutingProtocol::UpdateRouteToNeighbor (Ipv4Address sender, Ipv4Address receiver)
    {
      NS_LOG_FUNCTION (this << "sender " << sender << " receiver " << receiver);
      RoutingTableEntry toNeighbor;
      if (!m_routingTable.LookupRoute (sender, toNeighbor))
        {
          Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (receiver));
          RoutingTableEntry newEntry (/*device=*/ dev, /*dst=*/ sender, /*know seqno=*/ false, /*seqno=*/ 0,
                                      /*iface=*/ m_ipv4->GetAddress (m_ipv4->GetInterfaceForAddress (receiver), 0),
                                      /*hops=*/ 1, /*next hop=*/ sender, /*lifetime=*/ m_activeRouteTimeout);
          m_routingTable.AddRoute (newEntry);
        }
      else
        {
          Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (receiver));
          if (toNeighbor.GetValidSeqNo () && (toNeighbor.GetHop () == 1) && (toNeighbor.GetOutputDevice () == dev))
            {
              toNeighbor.SetLifeTime (std::max (m_activeRouteTimeout, toNeighbor.GetLifeTime ()));
            }
          else
            {
              RoutingTableEntry newEntry (/*device=*/ dev, /*dst=*/ sender, /*know seqno=*/ false, /*seqno=*/ 0,
                                          /*iface=*/ m_ipv4->GetAddress (m_ipv4->GetInterfaceForAddress (receiver), 0),
                                          /*hops=*/ 1, /*next hop=*/ sender, /*lifetime=*/ std::max (m_activeRouteTimeout, toNeighbor.GetLifeTime ()));
              m_routingTable.Update (newEntry);
            }
        }

    }

    void
    RoutingProtocol::RecvRequest (Ptr<Packet> p, Ipv4Address receiver, Ipv4Address src)
    {

      if(judge_time <=  Simulator::Now().GetSeconds())
        setCongestionParam();

      NS_LOG_FUNCTION (this);
      RreqHeader rreqHeader;
      p->RemoveHeader (rreqHeader);
      uint32_t m_addr_recv=0;
      uint32_t m_addr_src=0;

      //オリジナルIPアドレスの取得
      Ipv4Address m_orgn = rreqHeader.GetOrigin ();
      uint32_t m_addr_orgn=m_orgn.Get();
  

      //宛先IPアドレスの取得
      Ipv4Address m_dst = rreqHeader.GetDst ();
      uint32_t m_addr_dst = m_dst.Get();

      //RREQを受信・送信したノードのIPアドレスを取得
      m_addr_recv=receiver.Get();
      m_addr_src=src.Get();

      //IPアドレスの10.0.0.XXを取得
      int last_num_recv = (m_addr_recv >> 0) & 0xff;
      int last_num_src = (m_addr_src >> 0) & 0xff;
      int last_num_dst = (m_addr_dst >> 0) & 0xff;
      int last_num_orgn = (m_addr_orgn >> 0) & 0xff;
      /*
        std::cout << "宛先IPアドレス: "
        << ((m_addr_dst >> 24) & 0xff) << "."
        << ((m_addr_dst >> 16) & 0xff) << "."
        << ((m_addr_dst >> 8) & 0xff) << "."
        << ((m_addr_dst >> 0) & 0xff) << " ";
      */

      int recv_nodeID = last_num_recv - 1;
      int dst_nodeIndex = dstIndex(last_num_dst - 1);
 
      //RREQ制御部

      if(last_num_recv>=226 && last_num_recv<=249){
        if(last_num_src>=226 && last_num_src<=249){
          //std::cout<<"地上ノード同士のRREQ転送を抑止します\n";
          return;
        }
      }

/*
*
*   recv_nodeID >= 96 が移動する追加ノード
*
*  normal == 2 ？
*
*  ground  0:たて 1:よこ 2:ななめ
*/
/*
  double move_time = Simulator::Now().GetSeconds() ;


  if(recv_nodeID >= 96 && normal >= 2){
   // if(dst_nodeIndex >= 2)
         // return;


    if(ground == 0){
      if(move_time <= 50)//50
        return;
    }else if(ground == 1){
      if(recv_nodeID >= 107){
        if(move_time <= 40)
          return;
      }else{
        if(move_time <= 25)//25{
          return;
      }
    }else if(ground == 2){
      if(recv_nodeID <=103){
        if(move_time <= 30)//40
          return;
      }else{
        if(move_time <= 45)//40
          return;
      }
    }
  }
*/

if(normal >= 2){
  int result = getMoveID(recv_nodeID);
// if(getMoveID(recv_nodeID) == 1 && move_time >= 35 && move_time < 40)
//ホップ数全部同じか 0:同じ 1:ペナルティあり(node固定) 2ペナルティあり(CAC) 3ペナルティあり(CAC+time制限)
//int normal = 1;

//地上ノードの配置 0:たて 1:よこ 2:ななめ
//int ground = 0;
  double move_time = Simulator::Now().GetSeconds() ;
  if(ground == 0){
    if(move_time >= 32 && move_time <= 37 && result == 1)
      return;
  }
}


      //if(last_num_recv==25)
      //std::cout<< "Time in RecvRequest(aodv-routing-protocol.cc) "<<Simulator::Now().GetSeconds()<<"\n";

      // A node ignores all RREQs received from any node in its blacklist
      RoutingTableEntry toPrev;
      if (m_routingTable.LookupRoute (src, toPrev))
        {
          if (toPrev.IsUnidirectional ())
            {
              NS_LOG_DEBUG ("Ignoring RREQ from node in blacklist");
              return;
            }
        }

      uint32_t id = rreqHeader.GetId ();
      Ipv4Address origin = rreqHeader.GetOrigin ();

      /*
       *  Node checks to determine whether it has received a RREQ with the same Originator IP Address and RREQ ID.
       *  If such a RREQ has been received, the node silently discards the newly received RREQ.
       */
      if (m_rreqIdCache.IsDuplicate (origin, id))
        {
          NS_LOG_DEBUG ("Ignoring RREQ due to duplicate");
          return;
        }

      //bool switch_flag=false;


      uint8_t i_d_r=0;

      // 2019/04/16　ここから分散ルーチング(宛先)
      uint8_t i_d_d=0;
      //切り替えるときはここからコメントアウト

      //RREQの受信
      for(i_d_d = 0;i_d_d < req_no_d;i_d_d++){
        if(req_list_d[i_d_d] == last_num_dst)
          //if(req_list_gd[last_num_recv][i_d_d] == last_num_dst)
          break;
      }

      //ここまで（ver2）
      /*
        if(i_d_d==req_no_d){
        //req_list_d[i_d_d] = last_num_dst;
        req_list_gd[last_num_recv][i_d_d] = last_num_dst;
        req_no_d++;
        }
  
        // switch_flag=true;
        */
      //ここまでコメントアウト
      //ここまで(宛先)

  
      uint8_t hop;
      //  if(switch_flag==false)
      // Increment RREQ hop count
      //    hop = rreqHeader.GetHopCount () + 1;
      //  else
      //同じノードを通ったRREQの順番でホップ数を操作
      int goukei=0;

      if(i_d_d >= 2){//よこのデータがよかったのは２
        hop = rreqHeader.GetHopCount () + 1 + i_d_r+i_d_d*add_hop;
    
        goukei=i_d_r+i_d_d*3;
        /*  }else if(i_d_d >= 1){
            hop = rreqHeader.GetHopCount () + 1 + i_d_r+i_d_d*0.5;
            goukei=i_d_r+i_d_d*1;
        */
      }else
        hop = rreqHeader.GetHopCount () + 1;

     //std::cout << "log_1/2(4) = " << logn(1.0/2.0,4.0) << "\n";

     // hop = rreqHeader.GetHopCount () + 1 + i_d_r+i_d_d*0.5;

//混雑度を利用したペナルティホップ
int penalty=0;
/*
if( cong[recv_nodeID] >= (double)(1024*1024/8)){
      penalty = 3;
}else if(cong[recv_nodeID] >= (double)(1024*1024/(8*2))){
        penalty = 2;
}else{
        penalty = 0;
}*/
/*
double  flow_weight=0.0;
for(int i=0;i<dst_nodeIndex;i++){
  flow_weight += weight[recv_nodeID][i];
}

double total_weight=0.0;
for(int i=0;i<6;i++){
  total_weight += weight[recv_nodeID][i];
}
//byte -> bit 
flow_weight = flow_weight * 8;
total_weight = total_weight * 8;
if(flow_weight >= (double)(1024*1024)){
        penalty = 10;

        std::cout<<"----------------------------\n";
        std::cout<<"Node " << recv_nodeID << " reached Limit. FlowID: "<< dst_nodeIndex <<"\n";
        std::cout<< "Time: " << Simulator::Now().GetSeconds() << "\n";
        printf("flow_weight: %.0lf\n",flow_weight);
        printf("total_weight: %.0lf\n",total_weight);
        std::cout<<"----------------------------\n";


}else if(flow_weight >= 500000){
        penalty = 5;
}else{
        penalty=0;
}
*/

penalty = setPenalty(recv_nodeID,dst_nodeIndex,1);
if(penalty < 0)
return;

//std::cout<<"flow_weight: "<< flow_weight <<"\n";

hop = rreqHeader.GetHopCount () + 1 + penalty;
      //全部おなじで
       if(normal == 0)
       hop = rreqHeader.GetHopCount () + 1;



      /*
        uint8_t hop_t1 = rreqHeader.GetHopCount () + 1;
  
        uint8_t hop_t2 = rreqHeader.GetHopCount () + 1;
        if(hop_t1!=hop_t2){
        std::cout<<"hop 1st "<<unsigned(hop);
        std::cout<<"  hop 2nd "<<unsigned(hop)<<"\n";
        }
      */
      goukei=i_d_r+i_d_d*1;

      //エラー回避のダミー
      if(goukei<-1){
        std::cout<<goukei<<last_num_orgn<<last_num_dst;
      }

      rreqHeader.SetHopCount (hop);


      /*
       *  When the reverse route is created or updated, the following actions on the route are also carried out:
       *  1. the Originator Sequence Number from the RREQ is compared to the corresponding destination sequence number
       *     in the route table entry and copied if greater than the existing value there
       *  2. the valid sequence number field is set to true;
       *  3. the next hop in the routing table becomes the node from which the  RREQ was received
       *  4. the hop count is copied from the Hop Count in the RREQ message;
       *  5. the Lifetime is set to be the maximum of (ExistingLifetime, MinimalLifetime), where
       *     MinimalLifetime = current time + 2*NetTraversalTime - 2*HopCount*NodeTraversalTime
       */
      RoutingTableEntry toOrigin;
      if (!m_routingTable.LookupRoute (origin, toOrigin))
        {
          Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (receiver));
          RoutingTableEntry newEntry (/*device=*/ dev, /*dst=*/ origin, /*validSeno=*/ true, /*seqNo=*/ rreqHeader.GetOriginSeqno (),
                                      /*iface=*/ m_ipv4->GetAddress (m_ipv4->GetInterfaceForAddress (receiver), 0), /*hops=*/ hop,
                                      /*nextHop*/ src, /*timeLife=*/ Time ((2 * m_netTraversalTime - 2 * hop * m_nodeTraversalTime)));
          m_routingTable.AddRoute (newEntry);
        }
      else
        {
          if (toOrigin.GetValidSeqNo ())
            {
              if (int32_t (rreqHeader.GetOriginSeqno ()) - int32_t (toOrigin.GetSeqNo ()) > 0)
                toOrigin.SetSeqNo (rreqHeader.GetOriginSeqno ());
            }
          else
            toOrigin.SetSeqNo (rreqHeader.GetOriginSeqno ());
          toOrigin.SetValidSeqNo (true);
          toOrigin.SetNextHop (src);
          toOrigin.SetOutputDevice (m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (receiver)));
          toOrigin.SetInterface (m_ipv4->GetAddress (m_ipv4->GetInterfaceForAddress (receiver), 0));
          toOrigin.SetHop (hop);
          toOrigin.SetLifeTime (std::max (Time (2 * m_netTraversalTime - 2 * hop * m_nodeTraversalTime),
                                          toOrigin.GetLifeTime ()));
          m_routingTable.Update (toOrigin);
          //m_nb.Update (src, Time (AllowedHelloLoss * HelloInterval));
        }


      RoutingTableEntry toNeighbor;
      if (!m_routingTable.LookupRoute (src, toNeighbor))
        {
          NS_LOG_DEBUG ("Neighbor:" << src << " not found in routing table. Creating an entry"); 
          Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (receiver));
          RoutingTableEntry newEntry (dev, src, false, rreqHeader.GetOriginSeqno (),
                                      m_ipv4->GetAddress (m_ipv4->GetInterfaceForAddress (receiver), 0),
                                      1, src, m_activeRouteTimeout);
          m_routingTable.AddRoute (newEntry);
        }
      else
        {
          toNeighbor.SetLifeTime (m_activeRouteTimeout);
          toNeighbor.SetValidSeqNo (false);
          toNeighbor.SetSeqNo (rreqHeader.GetOriginSeqno ()); 
          toNeighbor.SetFlag (VALID);
          toNeighbor.SetOutputDevice (m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (receiver)));
          toNeighbor.SetInterface (m_ipv4->GetAddress (m_ipv4->GetInterfaceForAddress (receiver), 0));
          toNeighbor.SetHop (1);
          toNeighbor.SetNextHop (src);
          m_routingTable.Update (toNeighbor);
        }
      m_nb.Update (src, Time (m_allowedHelloLoss * m_helloInterval));

      NS_LOG_LOGIC (receiver << " receive RREQ with hop count " << static_cast<uint32_t>(rreqHeader.GetHopCount ()) 
                    << " ID " << rreqHeader.GetId ()
                    << " to destination " << rreqHeader.GetDst ());

      //  A node generates a RREP if either:
      //  (i)  it is itself the destination,
      if (IsMyOwnAddress (rreqHeader.GetDst ()))
        {
          m_routingTable.LookupRoute (origin, toOrigin);
          NS_LOG_DEBUG ("Send reply since I am the destination");
          SendReply (rreqHeader, toOrigin);
          return;
        }
      /*
       * (ii) or it has an active route to the destination, the destination sequence number in the node's existing route table entry for the destination
       *      is valid and greater than or equal to the Destination Sequence Number of the RREQ, and the "destination only" flag is NOT set.
       */
      RoutingTableEntry toDst;
      Ipv4Address dst = rreqHeader.GetDst ();
      if (m_routingTable.LookupRoute (dst, toDst))
        {
          /*
           * Drop RREQ, This node RREP wil make a loop.
           */
          if (toDst.GetNextHop () == src)
            {
              NS_LOG_DEBUG ("Drop RREQ from " << src << ", dest next hop " << toDst.GetNextHop ());
              return;
            }
          /*
           * The Destination Sequence number for the requested destination is set to the maximum of the corresponding value
           * received in the RREQ message, and the destination sequence value currently maintained by the node for the requested destination.
           * However, the forwarding node MUST NOT modify its maintained value for the destination sequence number, even if the value
           * received in the incoming RREQ is larger than the value currently maintained by the forwarding node.
           */
          if ((rreqHeader.GetUnknownSeqno () || (int32_t (toDst.GetSeqNo ()) - int32_t (rreqHeader.GetDstSeqno ()) >= 0))
              && toDst.GetValidSeqNo () )
            {

              if(flag_check==true){
                std::cout<<"最初の表示 ";
                if(!d_only_debug)
                  std::cout<<"DestinationOnly  false\n\n";
                else
                  std::cout<<"DestinationOnly  true\n\n";
                flag_check=false;    
              }

              /*
                int last_num_dst = (m_addr_dst >> 0) & 0xff;
                int last_num_orgn = (m_addr_orgn >> 0) & 0xff;
              */
               double congestion = getCongestion(recv_nodeID,dst_nodeIndex);

               double t  = Simulator::Now().GetSeconds();
              // if(t >= 25 && t <= 35)
            //   std::cout << "congestion:" << congestion <<"\n"; 

                double th_time;
                if(ground==0){
                  th_time = 40.0;
                }else if(ground==1){
                  th_time = 25.0;
                }
              //フラグが正常に反映されないため力技で切り替え fanet 30 40
              if(!d_only_debug && ((t <= th_time || t >= th_time + 10.0)  || normal != 3 ||congestion<=1024*1024*3.0)/*&&(congestion<=1024*1024*2.5||normal!=2)*/){
                //std::cout<<"test: rreqHeader.GetDestinationOnly() -> "<< rreqHeader.GetDestinationOnly()<<"\n";
                if (!rreqHeader.GetDestinationOnly () && toDst.GetFlag () == VALID)
                  {
                    m_routingTable.LookupRoute (origin, toOrigin);
                    if( T_FLAG == 1 ){
                      //std::cout<<"ノード"<<last_num_recv-1<<"が宛先"<<last_num_dst-1<<",オリジン"<<last_num_orgn-1<<"で";
                      if(last_num_orgn-1 == 77){
                        routeID.push_back(last_num_recv-1);
                      }
                    }
                    //ここから
                    Ipv4Address toOrgn = toOrigin.GetNextHop ();
                    uint32_t m_addr_toOrgn = toOrgn.Get();
                    //IPアドレスの10.0.0.XXを取得
                    int last_num_toOrgn = (m_addr_toOrgn >> 0) & 0xff;
                    //std::cout<<"(toOrigin.GetNextHop() -> "<<last_num_toOrgn-1<<")";
                    //Simulator::Now().GetSeconds()
                    double push_time = Simulator::Now().GetSeconds();

                    replyRoute push_route={push_time,last_num_recv-1, - 1,last_num_toOrgn-1,last_num_dst-1,last_num_orgn-1};
                    reply_route.push_back(push_route);
                    //ここまで
                    //std::cout <<"SendReplyByIntermediateNode by " << last_num_recv-1 << " hop:"<<  static_cast<int16_t>(hop) <<"\n";
                    
                    SendReplyByIntermediateNode (toDst, toOrigin, rreqHeader.GetGratiousRrep ());
                    return;
                  }
              }else{
                std::cout<<".";
                ;
              }

              rreqHeader.SetDstSeqno (toDst.GetSeqNo ());
              rreqHeader.SetUnknownSeqno (false);
            }
        }

      SocketIpTtlTag tag;
      p->RemovePacketTag (tag);
      if (tag.GetTtl () < 2)
        {
          NS_LOG_DEBUG ("TTL exceeded. Drop RREQ origin " << src << " destination " << dst );
          return;
        }

      for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j =
             m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
        {
          Ptr<Socket> socket = j->first;
          Ipv4InterfaceAddress iface = j->second;
          Ptr<Packet> packet = Create<Packet> ();
          SocketIpTtlTag ttl;
          ttl.SetTtl (tag.GetTtl () - 1);
          packet->AddPacketTag (ttl);
          packet->AddHeader (rreqHeader);
          TypeHeader tHeader (AODVTYPE_RREQ);
          packet->AddHeader (tHeader);
          // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
          Ipv4Address destination;
          if (iface.GetMask () == Ipv4Mask::GetOnes ())
            {
              destination = Ipv4Address ("255.255.255.255");
            }
          else
            { 
              destination = iface.GetBroadcast ();
            }
          m_lastBcastTime = Simulator::Now ();
          Simulator::Schedule (Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0, 10))), &RoutingProtocol::SendTo, this, socket, packet, destination); 

        }
    }

    void
    RoutingProtocol::SendReply (RreqHeader const & rreqHeader, RoutingTableEntry const & toOrigin)
    {



      NS_LOG_FUNCTION (this << toOrigin.GetDestination ());
      /*
       * Destination node MUST increment its own sequence number by one if the sequence number in the RREQ packet is equal to that
       * incremented value. Otherwise, the destination does not change its sequence number before generating the  RREP message.
       */
      if (!rreqHeader.GetUnknownSeqno () && (rreqHeader.GetDstSeqno () == m_seqNo + 1))
        m_seqNo++;
      RrepHeader rrepHeader ( /*prefixSize=*/ 0, /*hops=*/ 0, /*dst=*/ rreqHeader.GetDst (),
                              /*dstSeqNo=*/ m_seqNo, /*origin=*/ toOrigin.GetDestination (), /*lifeTime=*/ m_myRouteTimeout);



      Ptr<Packet> packet = Create<Packet> ();
      SocketIpTtlTag tag;
      tag.SetTtl (toOrigin.GetHop ());
      packet->AddPacketTag (tag);
      packet->AddHeader (rrepHeader);
      TypeHeader tHeader (AODVTYPE_RREP);
      packet->AddHeader (tHeader);
      Ptr<Socket> socket = FindSocketWithInterfaceAddress (toOrigin.GetInterface ());
      NS_ASSERT (socket);
      socket->SendTo (packet, 0, InetSocketAddress (toOrigin.GetNextHop (), AODV_PORT));

      //ここから
      Ipv4Address toOrgn = toOrigin.GetNextHop ();
      uint32_t m_addr_toOrgn = toOrgn.Get();
      //IPアドレスの10.0.0.XXを取得
      int last_num_toOrgn = (m_addr_toOrgn >> 0) & 0xff;
      if(last_num_toOrgn == -1)
        std::cout<<"toOrigin.GetNextHop() -> "<<last_num_toOrgn-1<<"\n";
    }

    void
    RoutingProtocol::SendReplyByIntermediateNode (RoutingTableEntry & toDst, RoutingTableEntry & toOrigin, bool gratRep)
    {
      NS_LOG_FUNCTION (this);
      RrepHeader rrepHeader (/*prefix size=*/ 0, /*hops=*/ toDst.GetHop (), /*dst=*/ toDst.GetDestination (), /*dst seqno=*/ toDst.GetSeqNo (),
                             /*origin=*/ toOrigin.GetDestination (), /*lifetime=*/ toDst.GetLifeTime ());

      //確認

      Ipv4Address dst = rrepHeader.GetDst ();
      Ipv4Address orgn = rrepHeader.GetOrigin ();

      uint32_t m_addr_origin = orgn.Get();
      uint32_t m_addr_dst = dst.Get();

      int last_num_dst = (m_addr_dst >> 0) & 0xff;
      int last_num_orgn = (m_addr_origin >> 0) & 0xff;



      //確認ここまで
      if( T_FLAG == 2)
        std::cout<<"宛先を知ってるからRREPを送ります。dst:"<<last_num_dst-1<<" origin:"<<last_num_orgn-1<<"\n";


      /* If the node we received a RREQ for is a neighbor we are
       * probably facing a unidirectional link... Better request a RREP-ack
       */
      if (toDst.GetHop () == 1)
        {
          rrepHeader.SetAckRequired (true);
          RoutingTableEntry toNextHop;
          m_routingTable.LookupRoute (toOrigin.GetNextHop (), toNextHop);
          toNextHop.m_ackTimer.SetFunction (&RoutingProtocol::AckTimerExpire, this);
          toNextHop.m_ackTimer.SetArguments (toNextHop.GetDestination (), m_blackListTimeout);
          toNextHop.m_ackTimer.SetDelay (m_nextHopWait);
        }
      toDst.InsertPrecursor (toOrigin.GetNextHop ());
      toOrigin.InsertPrecursor (toDst.GetNextHop ());
      m_routingTable.Update (toDst);
      m_routingTable.Update (toOrigin);

      Ptr<Packet> packet = Create<Packet> ();
      SocketIpTtlTag tag;
      tag.SetTtl (toOrigin.GetHop ());
      packet->AddPacketTag (tag);
      packet->AddHeader (rrepHeader);
      TypeHeader tHeader (AODVTYPE_RREP);
      packet->AddHeader (tHeader);
      Ptr<Socket> socket = FindSocketWithInterfaceAddress (toOrigin.GetInterface ());
      NS_ASSERT (socket);
      socket->SendTo (packet, 0, InetSocketAddress (toOrigin.GetNextHop (), AODV_PORT));

      //ここから
      Ipv4Address next = toOrigin.GetNextHop ();
      uint32_t m_addr_next = next.Get();
      //IPアドレスの10.0.0.XXを取得
      int last_num_next = (m_addr_next >> 0) & 0xff;
      if(last_num_next == -1)
        std::cout<<"toOrigin.GetNextHop() -> "<<last_num_next-1<<"\n";

      // Generating gratuitous RREPs
      if (gratRep)
        {
          RrepHeader gratRepHeader (/*prefix size=*/ 0, /*hops=*/ toOrigin.GetHop (), /*dst=*/ toOrigin.GetDestination (),
                                    /*dst seqno=*/ toOrigin.GetSeqNo (), /*origin=*/ toDst.GetDestination (),
                                    /*lifetime=*/ toOrigin.GetLifeTime ());
          Ptr<Packet> packetToDst = Create<Packet> ();
          SocketIpTtlTag gratTag;
          gratTag.SetTtl (toDst.GetHop ());
          packetToDst->AddPacketTag (gratTag);
          packetToDst->AddHeader (gratRepHeader);
          TypeHeader type (AODVTYPE_RREP);
          packetToDst->AddHeader (type);
          Ptr<Socket> socket = FindSocketWithInterfaceAddress (toDst.GetInterface ());
          NS_ASSERT (socket);
          NS_LOG_LOGIC ("Send gratuitous RREP " << packet->GetUid ());
          socket->SendTo (packetToDst, 0, InetSocketAddress (toDst.GetNextHop (), AODV_PORT));
        }
    }

    void
    RoutingProtocol::SendReplyAck (Ipv4Address neighbor)
    {
      NS_LOG_FUNCTION (this << " to " << neighbor);
      RrepAckHeader h;
      TypeHeader typeHeader (AODVTYPE_RREP_ACK);
      Ptr<Packet> packet = Create<Packet> ();
      SocketIpTtlTag tag;
      tag.SetTtl (1);
      packet->AddPacketTag (tag);
      packet->AddHeader (h);
      packet->AddHeader (typeHeader);
      RoutingTableEntry toNeighbor;
      m_routingTable.LookupRoute (neighbor, toNeighbor);
      Ptr<Socket> socket = FindSocketWithInterfaceAddress (toNeighbor.GetInterface ());
      NS_ASSERT (socket);
      socket->SendTo (packet, 0, InetSocketAddress (neighbor, AODV_PORT));
    }

    void
    RoutingProtocol::RecvReply (Ptr<Packet> p, Ipv4Address receiver, Ipv4Address sender)
    {
      NS_LOG_FUNCTION (this << " src " << sender);
      RrepHeader rrepHeader;
      p->RemoveHeader (rrepHeader);
      Ipv4Address dst = rrepHeader.GetDst ();
      NS_LOG_LOGIC ("RREP destination " << dst << " RREP origin " << rrepHeader.GetOrigin ());
 


      uint32_t m_addr_recv=0;
      uint32_t m_addr_snd=0;
      uint32_t m_addr_dst = dst.Get();

      Ipv4Address orgn = rrepHeader.GetOrigin ();
      uint32_t m_addr_origin = orgn.Get();
      //RREPを受信・送信したノードのIPアドレスを取得
      m_addr_recv=receiver.Get();
      m_addr_snd=sender.Get();

      //IPアドレスの10.0.0.XXを取得
      int last_num_recv = (m_addr_recv >> 0) & 0xff;
      int last_num_snd = (m_addr_snd >> 0) & 0xff;
      int last_num_dst = (m_addr_dst >> 0) & 0xff;
      int last_num_orgn = (m_addr_origin >> 0) & 0xff;

      //if(last_num_snd!=last_num_orgn||last_num_snd!=last_num_orgn)
 


      //uint8_t hop = rrepHeader.GetHopCount () + 1;
      uint8_t hop;
      uint8_t i_d_r=0;
      uint8_t i_d_d=0;

      int penalty = setPenalty(last_num_recv-1,dstIndex(last_num_dst-1),0);
     if(penalty < 0)
        return;

      //オリジナルここまで
      // If RREP is Hello message
      if (dst == rrepHeader.GetOrigin ())
        {
          ProcessHello (rrepHeader, receiver);
          //std::cout<<" hello by "<<last_num_dst-1<<"\n";
          return;
        }

     if(last_num_recv==-1)
        std::cout<<"送："<< last_num_snd - 1 <<" 受："<< last_num_recv-1 <<" 宛:"<<last_num_dst-1<<" オリジン："<<last_num_orgn -1 << "\n";

      //ここに実装
      i_d_d=0;
      //切り替えるときはここからコメントアウト

      //RREQの受信
      for(i_d_d = 0;i_d_d < req_no_d;i_d_d++){
        if(req_list_d[i_d_d] == last_num_dst)
          //if(req_list_gd[last_num_recv][i_d_d] == last_num_dst)
          break;
      }
      if(i_d_d==req_no_d){
        req_list_d[i_d_d] = last_num_dst;
        //req_list_gd[last_num_recv][i_d_d] = last_num_dst;
        req_no_d++;
        //std::cout<<"ノード "<<last_num_recv-1<<" における新しい宛先 "<<last_num_dst-1<<" を追加\n";
      }
 
      if(last_num_recv==-1)
        std::cout<<i_d_r<<i_d_d<<"\n";
      // switch_flag=true;
      //std::cout<<"送："<< last_num_snd - 1 <<" 受："<< last_num_recv-1 <<" 宛:"<<last_num_dst-1<<" オリジン："<<last_num_orgn -1<<"\n";

      //ここまで


     // hop = rrepHeader.GetHopCount () + 1;
       if(i_d_d >= 2){//よこのデータがよかったのは２
        hop = rrepHeader.GetHopCount () + 1 +i_d_d*add_hop;
      }else
        hop = rrepHeader.GetHopCount () + 1;

     // hop = rrepHeader.GetHopCount () + 1 + i_d_d*0.5;

//全部同じ
      hop = rrepHeader.GetHopCount() + 1 + penalty;
      if(normal==0)
      hop = rrepHeader.GetHopCount () + 1;

      rrepHeader.SetHopCount (hop);

      /*
       * If the route table entry to the destination is created or updated, then the following actions occur:
       * -  the route is marked as active,
       * -  the destination sequence number is marked as valid,
       * -  the next hop in the route entry is assigned to be the node from which the RREP is received,
       *    which is indicated by the source IP address field in the IP header,
       * -  the hop count is set to the value of the hop count from RREP message + 1
       * -  the expiry time is set to the current time plus the value of the Lifetime in the RREP message,
       * -  and the destination sequence number is the Destination Sequence Number in the RREP message.
       */
      Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (receiver));
      RoutingTableEntry newEntry (/*device=*/ dev, /*dst=*/ dst, /*validSeqNo=*/ true, /*seqno=*/ rrepHeader.GetDstSeqno (),
                                  /*iface=*/ m_ipv4->GetAddress (m_ipv4->GetInterfaceForAddress (receiver), 0),/*hop=*/ hop,
                                  /*nextHop=*/ sender, /*lifeTime=*/ rrepHeader.GetLifeTime ());
      RoutingTableEntry toDst;
      if (m_routingTable.LookupRoute (dst, toDst))
        {
          /*
           * The existing entry is updated only in the following circumstances:
           * (i) the sequence number in the routing table is marked as invalid in route table entry.
           */
          if (!toDst.GetValidSeqNo ())
            {
              m_routingTable.Update (newEntry);
            }
          // (ii)the Destination Sequence Number in the RREP is greater than the node's copy of the destination sequence number and the known value is valid,
          else if ((int32_t (rrepHeader.GetDstSeqno ()) - int32_t (toDst.GetSeqNo ())) > 0)
            {
              m_routingTable.Update (newEntry);
            }
          else
            {
              // (iii) the sequence numbers are the same, but the route is marked as inactive.
              if ((rrepHeader.GetDstSeqno () == toDst.GetSeqNo ()) && (toDst.GetFlag () != VALID))
                {
                  m_routingTable.Update (newEntry);
                }
              // (iv)  the sequence numbers are the same, and the New Hop Count is smaller than the hop count in route table entry.
              else if ((rrepHeader.GetDstSeqno () == toDst.GetSeqNo ()) && (hop < toDst.GetHop ()))
                {
                  m_routingTable.Update (newEntry);
                }
            }
        }
      else
        {
          // The forward route for this destination is created if it does not already exist.
          NS_LOG_LOGIC ("add new route");
          m_routingTable.AddRoute (newEntry);
        }
      // Acknowledge receipt of the RREP by sending a RREP-ACK message back
      if (rrepHeader.GetAckRequired ())
        {
          SendReplyAck (sender);
          rrepHeader.SetAckRequired (false);
        }
      NS_LOG_LOGIC ("receiver " << receiver << " origin " << rrepHeader.GetOrigin ());

      //リストに追加
      if(last_num_orgn-1 == 77){
        routeID.push_back(last_num_recv-1);
      }

      if (IsMyOwnAddress (rrepHeader.GetOrigin ()))
        {
          //std::cout<<" RREPがオリジネータに到達しました hop="<< static_cast<int16_t>(hop) <<"\n";
          if( T_FLAG == 1){
            std::cout<<" RREP到達 hop="<< static_cast<int16_t>(hop) <<"\n";
            //std::cout<<"送："<< last_num_snd - 1 <<" 受："<< last_num_recv-1 <<" 宛:"<<last_num_dst-1<<" オリジン："<<last_num_orgn -1<<"\n";
            if(last_num_recv-1 >=  START_GND && last_num_recv-1 <= START_GND + 5){
              //for(itr = routeID.begin();itr != routeID.end();itr++){
              //std::cout <<*itr << " -> ";
              std::cout<<" RREP到達 hop="<< static_cast<int16_t>(hop) <<"\n";
              int route_size = (int)reply_route.size();
              //std::cout<<"route_size: "<< route_size <<"\n";
              int beforeID = -1;
              int ini_beforeID = -1;
              for(int loop_print=0;loop_print < route_size;loop_print++){
                if( beforeID == -1 && reply_route[loop_print].preID == -1){
                  if(ini_beforeID == -1){
                    ini_beforeID = reply_route[loop_print].recv_nodeID;
                  }
                  beforeID = reply_route[loop_print].recv_nodeID;
                  //std::cout << "start "<<reply_route[loop_print].recv_nodeID << " from " << reply_route[loop_print].preID <<"\n";
                }else if(beforeID == reply_route[loop_print].preID){
                  //std::cout << reply_route[loop_print].recv_nodeID << " from " << reply_route[loop_print].preID <<"\n";
                  beforeID = reply_route[loop_print].recv_nodeID;
                }
              }

            
              //std::cout <<  "end\n";
              routeID.clear();
              //ここで表示した分をvectorから削除する必要あり  ini_beforeIDを使いたい
              //reply_route.clear();
              removeFromVector(ini_beforeID,last_num_orgn -1,last_num_dst-1);
              std::cout << "RecvReply time: " << Simulator::Now().GetSeconds() << "\n" ;
            }
          }
          if (toDst.GetFlag () == IN_SEARCH)
            {
              m_routingTable.Update (newEntry);
              m_addressReqTimer[dst].Remove ();
              m_addressReqTimer.erase (dst);
            }
          m_routingTable.LookupRoute (dst, toDst);
          SendPacketFromQueue (dst, toDst.GetRoute ());
          return;
        }

      RoutingTableEntry toOrigin;
      if (!m_routingTable.LookupRoute (rrepHeader.GetOrigin (), toOrigin) || toOrigin.GetFlag () == IN_SEARCH)
        {
          return; // Impossible! drop.
        }
      toOrigin.SetLifeTime (std::max (m_activeRouteTimeout, toOrigin.GetLifeTime ()));
      m_routingTable.Update (toOrigin);

      // Update information about precursors
      if (m_routingTable.LookupValidRoute (rrepHeader.GetDst (), toDst))
        {
          toDst.InsertPrecursor (toOrigin.GetNextHop ());
          m_routingTable.Update (toDst);

          RoutingTableEntry toNextHopToDst;
          m_routingTable.LookupRoute (toDst.GetNextHop (), toNextHopToDst);
          toNextHopToDst.InsertPrecursor (toOrigin.GetNextHop ());
          m_routingTable.Update (toNextHopToDst);

          toOrigin.InsertPrecursor (toDst.GetNextHop ());
          m_routingTable.Update (toOrigin);

          RoutingTableEntry toNextHopToOrigin;
          m_routingTable.LookupRoute (toOrigin.GetNextHop (), toNextHopToOrigin);
          toNextHopToOrigin.InsertPrecursor (toDst.GetNextHop ());
          m_routingTable.Update (toNextHopToOrigin);
        }
      SocketIpTtlTag tag;
      p->RemovePacketTag(tag);
      if (tag.GetTtl () < 2)
        {
          NS_LOG_DEBUG ("TTL exceeded. Drop RREP destination " << dst << " origin " << rrepHeader.GetOrigin ());
          return;
        }

      //normal last_num_dst-1
//double sendBy_time = 30.0;
//double reply_time[6]={0.0,0.0,0.0,0.0,0.0,0.0};
//       dstIndex(int last_num_dst
  
      

      Ptr<Packet> packet = Create<Packet> ();
      SocketIpTtlTag ttl;
      ttl.SetTtl (tag.GetTtl() - 1);
      packet->AddPacketTag (ttl);
      packet->AddHeader (rrepHeader);
      TypeHeader tHeader (AODVTYPE_RREP);
      packet->AddHeader (tHeader);
      Ptr<Socket> socket = FindSocketWithInterfaceAddress (toOrigin.GetInterface ());
      NS_ASSERT (socket);
      socket->SendTo (packet, 0, InetSocketAddress (toOrigin.GetNextHop (), AODV_PORT));
      //ここから
      if(last_num_orgn -1 >= START_GND && last_num_orgn -1 <= START_GND + 5){
        Ipv4Address next = toOrigin.GetNextHop ();
        uint32_t m_addr_next = next.Get();
        //IPアドレスの10.0.0.XXを取得
        int last_num_next = (m_addr_next >> 0) & 0xff;
        //std::cout<<"toOrigin.GetNextHop() -> "<<last_num_next-1<<" from "<<last_num_recv-1<<"\n";
        //Simulator::Now().GetSeconds()
        double push_time = Simulator::Now().GetSeconds();

        replyRoute push_route={push_time,last_num_recv-1,last_num_snd - 1,last_num_next-1,last_num_dst-1,last_num_orgn -1};
        reply_route.push_back(push_route);
      }
    }

    void
    RoutingProtocol::RecvReplyAck (Ipv4Address neighbor)
    {
      NS_LOG_FUNCTION (this);
      RoutingTableEntry rt;
      if(m_routingTable.LookupRoute (neighbor, rt))
        {
          rt.m_ackTimer.Cancel ();
          rt.SetFlag (VALID);
          m_routingTable.Update (rt);
        }
    }

    void
    RoutingProtocol::ProcessHello (RrepHeader const & rrepHeader, Ipv4Address receiver )
    {
      NS_LOG_FUNCTION (this << "from " << rrepHeader.GetDst ());
      /*
       *  Whenever a node receives a Hello message from a neighbor, the node
       * SHOULD make sure that it has an active route to the neighbor, and
       * create one if necessary.
       */
      RoutingTableEntry toNeighbor;
      if (!m_routingTable.LookupRoute (rrepHeader.GetDst (), toNeighbor))
        {
          Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (receiver));
          RoutingTableEntry newEntry (/*device=*/ dev, /*dst=*/ rrepHeader.GetDst (), /*validSeqNo=*/ true, /*seqno=*/ rrepHeader.GetDstSeqno (),
                                      /*iface=*/ m_ipv4->GetAddress (m_ipv4->GetInterfaceForAddress (receiver), 0),
                                      /*hop=*/ 1, /*nextHop=*/ rrepHeader.GetDst (), /*lifeTime=*/ rrepHeader.GetLifeTime ());
          m_routingTable.AddRoute (newEntry);
        }
      else
        {
          toNeighbor.SetLifeTime (std::max (Time (m_allowedHelloLoss * m_helloInterval), toNeighbor.GetLifeTime ()));
          toNeighbor.SetSeqNo (rrepHeader.GetDstSeqno ());
          toNeighbor.SetValidSeqNo (true);
          toNeighbor.SetFlag (VALID);
          toNeighbor.SetOutputDevice (m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (receiver)));
          toNeighbor.SetInterface (m_ipv4->GetAddress (m_ipv4->GetInterfaceForAddress (receiver), 0));
          toNeighbor.SetHop (1);
          toNeighbor.SetNextHop (rrepHeader.GetDst ());
          m_routingTable.Update (toNeighbor);
        }
      if (m_enableHello)
        {
          m_nb.Update (rrepHeader.GetDst (), Time (m_allowedHelloLoss * m_helloInterval));
        }
    }

    void
    RoutingProtocol::RecvError (Ptr<Packet> p, Ipv4Address src )
    {
      NS_LOG_FUNCTION (this << " from " << src);
      RerrHeader rerrHeader;
      p->RemoveHeader (rerrHeader);
      std::map<Ipv4Address, uint32_t> dstWithNextHopSrc;
      std::map<Ipv4Address, uint32_t> unreachable;
      m_routingTable.GetListOfDestinationWithNextHop (src, dstWithNextHopSrc);
      std::pair<Ipv4Address, uint32_t> un;
      while (rerrHeader.RemoveUnDestination (un))
        {
          for (std::map<Ipv4Address, uint32_t>::const_iterator i =
                 dstWithNextHopSrc.begin (); i != dstWithNextHopSrc.end (); ++i)
            {
              if (i->first == un.first)
                {
                  unreachable.insert (un);
                }
            }
        }

      std::vector<Ipv4Address> precursors;
      for (std::map<Ipv4Address, uint32_t>::const_iterator i = unreachable.begin ();
           i != unreachable.end ();)
        {
          if (!rerrHeader.AddUnDestination (i->first, i->second))
            {
              TypeHeader typeHeader (AODVTYPE_RERR);
              Ptr<Packet> packet = Create<Packet> ();
              SocketIpTtlTag tag;
              tag.SetTtl (1);
              packet->AddPacketTag (tag);
              packet->AddHeader (rerrHeader);
              packet->AddHeader (typeHeader);
              SendRerrMessage (packet, precursors);
              rerrHeader.Clear ();
            }
          else
            {
              RoutingTableEntry toDst;
              m_routingTable.LookupRoute (i->first, toDst);
              toDst.GetPrecursors (precursors);
              ++i;
            }
        }
      if (rerrHeader.GetDestCount () != 0)
        {
          TypeHeader typeHeader (AODVTYPE_RERR);
          Ptr<Packet> packet = Create<Packet> ();
          SocketIpTtlTag tag;
          tag.SetTtl (1);
          packet->AddPacketTag (tag);
          packet->AddHeader (rerrHeader);
          packet->AddHeader (typeHeader);
          SendRerrMessage (packet, precursors);
        }
      m_routingTable.InvalidateRoutesWithDst (unreachable);
    }

    void
    RoutingProtocol::RouteRequestTimerExpire (Ipv4Address dst)
    {
      NS_LOG_LOGIC (this);
      RoutingTableEntry toDst;
      if (m_routingTable.LookupValidRoute (dst, toDst))
        {
          SendPacketFromQueue (dst, toDst.GetRoute ());
          NS_LOG_LOGIC ("route to " << dst << " found");
          return;
        }
      /*
       *  If a route discovery has been attempted RreqRetries times at the maximum TTL without
       *  receiving any RREP, all data packets destined for the corresponding destination SHOULD be
       *  dropped from the buffer and a Destination Unreachable message SHOULD be delivered to the application.
       */
      if (toDst.GetRreqCnt () == m_rreqRetries)
        {
          NS_LOG_LOGIC ("route discovery to " << dst << " has been attempted RreqRetries (" << m_rreqRetries << ") times with ttl " << m_netDiameter);
          m_addressReqTimer.erase (dst);
          m_routingTable.DeleteRoute (dst);
          NS_LOG_DEBUG ("Route not found. Drop all packets with dst " << dst);
          m_queue.DropPacketWithDst (dst);
          return;
        }

      if (toDst.GetFlag () == IN_SEARCH)
        {
          NS_LOG_LOGIC ("Resend RREQ to " << dst << " previous ttl " << toDst.GetHop ());
          SendRequest (dst);
        }
      else
        {
          NS_LOG_DEBUG ("Route down. Stop search. Drop packet with destination " << dst);
          m_addressReqTimer.erase (dst);
          m_routingTable.DeleteRoute (dst);
          m_queue.DropPacketWithDst (dst);
        }
    }

    void
    RoutingProtocol::HelloTimerExpire ()
    {
      NS_LOG_FUNCTION (this);
      Time offset = Time (Seconds (0));
      if (m_lastBcastTime > Time (Seconds (0)))
        {
          offset = Simulator::Now () - m_lastBcastTime;
          NS_LOG_DEBUG ("Hello deferred due to last bcast at:" << m_lastBcastTime);
        }
      else
        {
          SendHello ();
        }
      m_htimer.Cancel ();
      Time diff = m_helloInterval - offset;
      m_htimer.Schedule (std::max (Time (Seconds (0)), diff));
      m_lastBcastTime = Time (Seconds (0));
    }

    void
    RoutingProtocol::RreqRateLimitTimerExpire ()
    {
      NS_LOG_FUNCTION (this);
      m_rreqCount = 0;
      m_rreqRateLimitTimer.Schedule (Seconds (1));
    }

    void
    RoutingProtocol::RerrRateLimitTimerExpire ()
    {
      NS_LOG_FUNCTION (this);
      m_rerrCount = 0;
      m_rerrRateLimitTimer.Schedule (Seconds (1));
    }

    void
    RoutingProtocol::AckTimerExpire (Ipv4Address neighbor, Time blacklistTimeout)
    {
      NS_LOG_FUNCTION (this);
      m_routingTable.MarkLinkAsUnidirectional (neighbor, blacklistTimeout);
    }

    void
    RoutingProtocol::SendHello ()
    {
      NS_LOG_FUNCTION (this);
      /* Broadcast a RREP with TTL = 1 with the RREP message fields set as follows:
       *   Destination IP Address         The node's IP address.
       *   Destination Sequence Number    The node's latest sequence number.
       *   Hop Count                      0
       *   Lifetime                       AllowedHelloLoss * HelloInterval
       */
      for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
        {
          Ptr<Socket> socket = j->first;
          Ipv4InterfaceAddress iface = j->second;
          RrepHeader helloHeader (/*prefix size=*/ 0, /*hops=*/ 0, /*dst=*/ iface.GetLocal (), /*dst seqno=*/ m_seqNo,
                                  /*origin=*/ iface.GetLocal (),/*lifetime=*/ Time (m_allowedHelloLoss * m_helloInterval));
          Ptr<Packet> packet = Create<Packet> ();
          SocketIpTtlTag tag;
          tag.SetTtl (1);
          packet->AddPacketTag (tag);
          packet->AddHeader (helloHeader);
          TypeHeader tHeader (AODVTYPE_RREP);
          packet->AddHeader (tHeader);
          // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
          Ipv4Address destination;
          if (iface.GetMask () == Ipv4Mask::GetOnes ())
            {
              destination = Ipv4Address ("255.255.255.255");
            }
          else
            { 
              destination = iface.GetBroadcast ();
            }
          Time jitter = Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0, 10)));
          Simulator::Schedule (jitter, &RoutingProtocol::SendTo, this , socket, packet, destination);
        }
    }

    void
    RoutingProtocol::SendPacketFromQueue (Ipv4Address dst, Ptr<Ipv4Route> route)
    {
      NS_LOG_FUNCTION (this);
      QueueEntry queueEntry;
      while (m_queue.Dequeue (dst, queueEntry))
        {
          DeferredRouteOutputTag tag;
          Ptr<Packet> p = ConstCast<Packet> (queueEntry.GetPacket ());
          if (p->RemovePacketTag (tag) && 
              tag.GetInterface() != -1 &&
              tag.GetInterface() != m_ipv4->GetInterfaceForDevice (route->GetOutputDevice ()))
            {
              NS_LOG_DEBUG ("Output device doesn't match. Dropped.");
              return;
            }
          UnicastForwardCallback ucb = queueEntry.GetUnicastForwardCallback ();
          Ipv4Header header = queueEntry.GetIpv4Header ();
          header.SetSource (route->GetSource ());
          header.SetTtl (header.GetTtl () + 1); // compensate extra TTL decrement by fake loopback routing
          ucb (route, p, header);
        }
    }

    void
    RoutingProtocol::SendRerrWhenBreaksLinkToNextHop (Ipv4Address nextHop)
    {
      NS_LOG_FUNCTION (this << nextHop);
      RerrHeader rerrHeader;
      std::vector<Ipv4Address> precursors;
      std::map<Ipv4Address, uint32_t> unreachable;

      RoutingTableEntry toNextHop;
      if (!m_routingTable.LookupRoute (nextHop, toNextHop))
        return;
      toNextHop.GetPrecursors (precursors);
      rerrHeader.AddUnDestination (nextHop, toNextHop.GetSeqNo ());
      m_routingTable.GetListOfDestinationWithNextHop (nextHop, unreachable);
      for (std::map<Ipv4Address, uint32_t>::const_iterator i = unreachable.begin (); i
             != unreachable.end ();)
        {
          if (!rerrHeader.AddUnDestination (i->first, i->second))
            {
              NS_LOG_LOGIC ("Send RERR message with maximum size.");
              TypeHeader typeHeader (AODVTYPE_RERR);
              Ptr<Packet> packet = Create<Packet> ();
              SocketIpTtlTag tag;
              tag.SetTtl (1);
              packet->AddPacketTag (tag);
              packet->AddHeader (rerrHeader);
              packet->AddHeader (typeHeader);
              SendRerrMessage (packet, precursors);
              rerrHeader.Clear ();
            }
          else
            {
              RoutingTableEntry toDst;
              m_routingTable.LookupRoute (i->first, toDst);
              toDst.GetPrecursors (precursors);
              ++i;
            }
        }
      if (rerrHeader.GetDestCount () != 0)
        {
          TypeHeader typeHeader (AODVTYPE_RERR);
          Ptr<Packet> packet = Create<Packet> ();
          SocketIpTtlTag tag;
          tag.SetTtl (1);
          packet->AddPacketTag (tag);
          packet->AddHeader (rerrHeader);
          packet->AddHeader (typeHeader);
          SendRerrMessage (packet, precursors);
        }
      unreachable.insert (std::make_pair (nextHop, toNextHop.GetSeqNo ()));
      m_routingTable.InvalidateRoutesWithDst (unreachable);
    }

    void
    RoutingProtocol::SendRerrWhenNoRouteToForward (Ipv4Address dst,
                                                   uint32_t dstSeqNo, Ipv4Address origin)
    {
      NS_LOG_FUNCTION (this);
      // A node SHOULD NOT originate more than RERR_RATELIMIT RERR messages per second.
      if (m_rerrCount == m_rerrRateLimit)
        {
          // Just make sure that the RerrRateLimit timer is running and will expire
          NS_ASSERT (m_rerrRateLimitTimer.IsRunning ());
          // discard the packet and return
          NS_LOG_LOGIC ("RerrRateLimit reached at " << Simulator::Now ().GetSeconds () << " with timer delay left " 
                        << m_rerrRateLimitTimer.GetDelayLeft ().GetSeconds ()
                        << "; suppressing RERR");
          return;
        }
      RerrHeader rerrHeader;
      rerrHeader.AddUnDestination (dst, dstSeqNo);
      RoutingTableEntry toOrigin;
      Ptr<Packet> packet = Create<Packet> ();
      SocketIpTtlTag tag;
      tag.SetTtl (1);
      packet->AddPacketTag (tag);
      packet->AddHeader (rerrHeader);
      packet->AddHeader (TypeHeader (AODVTYPE_RERR));
      if (m_routingTable.LookupValidRoute (origin, toOrigin))
        {
          Ptr<Socket> socket = FindSocketWithInterfaceAddress (
                                                               toOrigin.GetInterface ());
          NS_ASSERT (socket);
          NS_LOG_LOGIC ("Unicast RERR to the source of the data transmission");
          socket->SendTo (packet, 0, InetSocketAddress (toOrigin.GetNextHop (), AODV_PORT));
        }
      else
        {
          for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator i =
                 m_socketAddresses.begin (); i != m_socketAddresses.end (); ++i)
            {
              Ptr<Socket> socket = i->first;
              Ipv4InterfaceAddress iface = i->second;
              NS_ASSERT (socket);
              NS_LOG_LOGIC ("Broadcast RERR message from interface " << iface.GetLocal ());
              // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
              Ipv4Address destination;
              if (iface.GetMask () == Ipv4Mask::GetOnes ())
                {
                  destination = Ipv4Address ("255.255.255.255");
                }
              else
                { 
                  destination = iface.GetBroadcast ();
                }
              socket->SendTo (packet->Copy (), 0, InetSocketAddress (destination, AODV_PORT));
            }
        }
    }

    void
    RoutingProtocol::SendRerrMessage (Ptr<Packet> packet, std::vector<Ipv4Address> precursors)
    {
      NS_LOG_FUNCTION (this);

      if (precursors.empty ())
        {
          NS_LOG_LOGIC ("No precursors");
          return;
        }
      // A node SHOULD NOT originate more than RERR_RATELIMIT RERR messages per second.
      if (m_rerrCount == m_rerrRateLimit)
        {
          // Just make sure that the RerrRateLimit timer is running and will expire
          NS_ASSERT (m_rerrRateLimitTimer.IsRunning ());
          // discard the packet and return
          NS_LOG_LOGIC ("RerrRateLimit reached at " << Simulator::Now ().GetSeconds () << " with timer delay left " 
                        << m_rerrRateLimitTimer.GetDelayLeft ().GetSeconds ()
                        << "; suppressing RERR");
          return;
        }
      // If there is only one precursor, RERR SHOULD be unicast toward that precursor
      if (precursors.size () == 1)
        {
          RoutingTableEntry toPrecursor;
          if (m_routingTable.LookupValidRoute (precursors.front (), toPrecursor))
            {
              Ptr<Socket> socket = FindSocketWithInterfaceAddress (toPrecursor.GetInterface ());
              NS_ASSERT (socket);
              NS_LOG_LOGIC ("one precursor => unicast RERR to " << toPrecursor.GetDestination () << " from " << toPrecursor.GetInterface ().GetLocal ());
              Simulator::Schedule (Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0, 10))), &RoutingProtocol::SendTo, this, socket, packet, precursors.front ());
              m_rerrCount++;
            }
          return;
        }

      //  Should only transmit RERR on those interfaces which have precursor nodes for the broken route
      std::vector<Ipv4InterfaceAddress> ifaces;
      RoutingTableEntry toPrecursor;
      for (std::vector<Ipv4Address>::const_iterator i = precursors.begin (); i != precursors.end (); ++i)
        {
          if (m_routingTable.LookupValidRoute (*i, toPrecursor) && 
              std::find (ifaces.begin (), ifaces.end (), toPrecursor.GetInterface ()) == ifaces.end ())
            {
              ifaces.push_back (toPrecursor.GetInterface ());
            }
        }

      for (std::vector<Ipv4InterfaceAddress>::const_iterator i = ifaces.begin (); i != ifaces.end (); ++i)
        {
          Ptr<Socket> socket = FindSocketWithInterfaceAddress (*i);
          NS_ASSERT (socket);
          NS_LOG_LOGIC ("Broadcast RERR message from interface " << i->GetLocal ());
          // std::cout << "Broadcast RERR message from interface " << i->GetLocal () << std::endl;
          // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
          Ptr<Packet> p = packet->Copy ();
          Ipv4Address destination;
          if (i->GetMask () == Ipv4Mask::GetOnes ())
            {
              destination = Ipv4Address ("255.255.255.255");
            }
          else
            { 
              destination = i->GetBroadcast ();
            }
          Simulator::Schedule (Time (MilliSeconds (m_uniformRandomVariable->GetInteger (0, 10))), &RoutingProtocol::SendTo, this, socket, p, destination);
        }
    }

    Ptr<Socket>
    RoutingProtocol::FindSocketWithInterfaceAddress (Ipv4InterfaceAddress addr ) const
    {
      NS_LOG_FUNCTION (this << addr);
      for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j =
             m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
        {
          Ptr<Socket> socket = j->first;
          Ipv4InterfaceAddress iface = j->second;
          if (iface == addr)
            return socket;
        }
      Ptr<Socket> socket;
      return socket;
    }

    Ptr<Socket>
    RoutingProtocol::FindSubnetBroadcastSocketWithInterfaceAddress (Ipv4InterfaceAddress addr ) const
    {
      NS_LOG_FUNCTION (this << addr);
      for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j =
             m_socketSubnetBroadcastAddresses.begin (); j != m_socketSubnetBroadcastAddresses.end (); ++j)
        {
          Ptr<Socket> socket = j->first;
          Ipv4InterfaceAddress iface = j->second;
          if (iface == addr)
            return socket;
        }
      Ptr<Socket> socket;
      return socket;
    }

    void
    RoutingProtocol::DoInitialize (void)
    {
      NS_LOG_FUNCTION (this);
      uint32_t startTime;
      if (m_enableHello)
        {
          m_htimer.SetFunction (&RoutingProtocol::HelloTimerExpire, this);
          startTime = m_uniformRandomVariable->GetInteger (0, 100);
          NS_LOG_DEBUG ("Starting at time " << startTime << "ms");
          m_htimer.Schedule (MilliSeconds (startTime));
        }
      Ipv4RoutingProtocol::DoInitialize ();
    }

  } //namespace aodv
} //namespace ns3
