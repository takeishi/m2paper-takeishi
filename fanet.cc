/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Vikas Pushkar (Adapted from third.cc)
 */

/*
  26/02/2019

  大阪府某所の避難所のおおよその距離を使用したシミュレーションシナリオ

*/


#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/csma-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/internet-module.h"
#include "ns3/netanim-module.h"
#include "ns3/basic-energy-source.h"
#include "ns3/simple-device-energy-model.h"

#include "ns3/node.h"

#include "ns3/aodv-module.h"
#include "ns3/v4ping-helper.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <stdio.h>
#include <time.h>
#include "ns3/wifi-phy.h"
#include "ns3/packet.h"
#include <stdlib.h>

#include "ns3/ipv4-l3-protocol.h"
#include "ns3/header.h"
#include "ns3/aodv-routing-protocol.h"
#include "ns3/ipv4-header.h"
#include "ns3/packet-socket.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/aodv-packet.h"
#include "ns3/wifi-mac.h"
#include "ns3/wifi-mac-queue.h"
#include "ns3/seq-ts-header.h"
#include "ns3/takeishi-header.h"
#include "ns3/packet-metadata.h"

//遅延取得？？
#include "ns3/flow-monitor-helper.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/ipv4-flow-classifier.h"

//地上ノードの配置・アプリケーションの向き
//1たて　2よこ　3ななめ
#define G_TYPE 1

//FANETトポロジ
//1:Grid  2:Triangle  3:Honeycomb
#define TOPO_TYPE 1

//移動UAVの有無
//0:なし 1:あり
#define MOVE_FLAG 1

#define BUF 256

#define NODE 200

double cong[NODE];

double weight[NODE][6];

//一定時間を区切るための変数
float event_time=10.0;
//#define NODE NODE
//long int recvByte_in_Time[NODE]={0};

int dstIndex(int last_num_dst){
  int baseIndex;
  if(G_TYPE != 2){
    baseIndex = 72;
  }else{
    baseIndex = 84;
  }

  if(last_num_dst >= baseIndex && last_num_dst <= baseIndex + 5)
    return last_num_dst - baseIndex;
  else
    return -1;
}

struct dstTime{
  double recv_time;
  long int recvByte_in_Time;
};

struct dstTime dst_time[NODE];

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("Fanet");

//シミュレーションクラス(作成:2018/10/30)
class FanetSimulation
{
public:
  FanetSimulation();
  /// Configure script parameters, \return true on successful configuration
  bool Configure (int argc, char **argv);
  /// Run simulation
  void RunSimu();
  /// Report results
  void Report (std::ostream & os);

  //private:

  // parameters
  /// ノード数その１
  uint32_t size;
  // ノード数その２
  uint32_t size2;
  //UAVノード数
  uint32_t uav;
  /// ノード間距離（メートル）
  double step;
  /// シミュレーション時間（秒）
  double totalTime;
  /// Write per-device PCAP traces if true
  bool pcap;
  /// Print routes if true
  bool printRoutes;

 
public:
  // network
  NodeContainer nodes;//すべてのノードが属するコンテナ
  NodeContainer gnds;//追加、地上ノードその１ 葦原小学校
  NodeContainer gnds2;//追加、地上ノードその２ ゆめあいセンター
  NodeContainer gnds3;//追加、地上ノードその３ 玉櫛公民館
  NodeContainer gnds4;//追加、地上ノードその４ 南市民体育館
  NodeContainer uavs;//追加、UAVノードコンテナ

  NodeContainer move1;
  ;

  NetDeviceContainer devices;
  Ipv4InterfaceContainer interfaces;
  Address serverAddress;
  WifiMacHelper wifiMac;//移動、無線通信用？
  
  //private:
  void CreateNodes ();
  void CreateDevices ();
  void InstallInternetStack ();
  void InstallApplications ();
};

//コンストラクタ  それぞれの値でメンバ変数を初期化
//作成 2018/10/30
FanetSimulation::FanetSimulation():
  size (6),//地上ノード数
  size2 (6),//もう一方の地上ノード数
  uav (72),//追加　UAVノード数
  step (100),
  totalTime (80),//もともとは65秒
  pcap (true),//default true
  printRoutes (true)
{
  std::cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  std::cout<<"以下の項目について設定を確認しましたか？\n";
  std::cout<<"地上ノードの配置・アプリケーションの向き G_TYPE:" << G_TYPE <<"\n";
  std::cout<<"FANETトポロジ TOPO_TYPE:" << TOPO_TYPE <<"\n";
  std::cout<<"移動UAVの有無 MOVE_FLAG:" << MOVE_FLAG <<"\n";
  std::cout<<"経路確認フラグ T_FLAG\n";
  std::cout<<"地上のノード配置によってかわるID START_GND\n";
  std::cout<<"routingのホップ数のルールも確認しましたか？\n";
  std::cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++\n";

  int i;
  for(i=0;i<NODE;i++){
    dst_time[i].recv_time = event_time;
    dst_time[i].recvByte_in_Time = 0;
  }

  for(i=0;i<NODE;i++){
    cong[i]=0.0;
    for(int j=0;j<6;j++){
      weight[i][j]=0.0;
    }
  }

  FILE *fp;
  char filename[BUF]={'\0'};
  //  char str[BUF]={'\0'};

  //ネーミングのベース部分
  strcpy(filename,"./src/aodv/model/congestion.txt");

      
  //ファイルポインタを設定
  fp = fopen(filename,"w");
  if(fp==NULL){
    printf("miss %s\n",filename);
    return;
  }

  for(int i=0;i<NODE;i++){
    char all_weight[BUF]={'\0'};
    char str_cong[BUF]={'\0'};
    sprintf(str_cong, "%.2lf",cong[i]);

    for(int j=0;j<6;j++){
      char str_weight[BUF]={'\0'};
      sprintf(str_weight, "%0.2lf",weight[i][j]);
      strcat(all_weight,str_weight);
      if(j<5)
        strcat(all_weight," ");  
    }
    //    fprintf(fp,"nodeID 混雑度 フロー1 フロー2 フロー3 フロー4 フロー5 フロー6",str);
    fprintf(fp,"Node%03d %s %s\n",i,str_cong,all_weight);
  }

  fclose(fp);

}

//設定関数　よくわかってない
//作成 2018/10/30
bool
FanetSimulation::Configure (int argc, char **argv)
{
  // Enable AODV logs by default. Comment this if too noisy
  // LogComponentEnable("AodvRoutingProtocol", LOG_LEVEL_ALL);

  SeedManager::SetSeed (12345);
  CommandLine cmd;

  cmd.AddValue ("pcap", "Write PCAP traces.", pcap);
  cmd.AddValue ("printRoutes", "Print routing table dumps.", printRoutes);
  cmd.AddValue ("size", "Number of nodes.", size);
  cmd.AddValue ("size2", "Number of nodes.", size2);
  cmd.AddValue ("uav","Number of uav nodes",uav);
  cmd.AddValue ("time", "Simulation time, s.", totalTime);
  cmd.AddValue ("step", "Grid step, m", step);

  cmd.Parse (argc, argv);
  return true;
}

//シミュレーション実行関数
//作成　2018/10/30
//NetAnim用xmlファイルも作成 2018/11/07
void
FanetSimulation::RunSimu()
{
  //  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", UintegerValue (1)); // enable rts cts all the time.
  CreateNodes ();
  CreateDevices ();
  InstallInternetStack ();
  InstallApplications ();

  std::cout << "Starting simulation for " << totalTime << " s ...\n";

  AnimationInterface anim ("a-fanet-bad.xml"); // Mandatory
  anim.EnablePacketMetadata (); // Optional
  anim.EnableIpv4RouteTracking ("routingtable-wireless.xml", Seconds (0), Seconds (5), Seconds (0.25)); //Optional
  anim.EnableWifiMacCounters (Seconds (0), Seconds (10)); //Optional
  anim.EnableWifiPhyCounters (Seconds (0), Seconds (10)); //Optional

  //トレースファイル作成
  //AsciiTraceHelper ascii;
  //devices.EnableAsciiAll(ascii.CreateFileStream("fanet.tr"));

  Simulator::Stop (Seconds (totalTime));
  Simulator::Run ();
  Simulator::Destroy ();
}

//報告関数　使ってない？
//作成 2018/10/30
void
FanetSimulation::Report (std::ostream &)
{ 
}

//ノード作成とトポロジ設定
//作成　2018/10/30
void
FanetSimulation::CreateNodes ()
{

  uint32_t sum= (unsigned)size+(unsigned)uav+(unsigned)size2; 
  std::cout << "Creating " << (unsigned)sum << " nodes " << step << " m apart.\n";
  //nodes.Create (size);

  //UAVノード作成→地上ノード5こずつ作成
  uavs.Create(uav);
  gnds.Create(size);//追加、地上ノードその１ 葦原小学校
  gnds2.Create(size2);//追加、地上ノードその２ ゆめあいセンター
  gnds3.Create(size);//追加、地上ノードその３ 玉櫛公民館
  gnds4.Create(size);//追加、地上ノードその４ 南市民体育館
  move1.Create(23);

  
  //作成したノードをそれぞれ全体ノードコンテナに追加
  nodes.Add(uavs);
  nodes.Add(gnds);
  nodes.Add(gnds2);
  nodes.Add(gnds3);
  nodes.Add(gnds4);
  nodes.Add(move1);

  
  //uint32_t sum= (unsigned)size+(unsigned)uav;
  
  // Name nodes
  for (uint32_t i = 0; i < sum; ++i)
    {
      std::ostringstream os;
      os << "node-" << i;
      Names::Add (os.str (), nodes.Get (i));
    }

  //地上ノードその１ 葦原小学校
  MobilityHelper mobilityGnd;

  switch(G_TYPE){
  case 1://たて
    mobilityGnd.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                      "X", StringValue("ns3::UniformRandomVariable[Min=595|Max=605]"), 
                                      "Y", StringValue("ns3::UniformRandomVariable[Min=65|Max=75]"), 
                                      "Z", StringValue("ns3::UniformRandomVariable[Min=1.2|Max=1.6]"));
    break;
  case 2://よこ(たてと同じ)
    mobilityGnd.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                      "X", StringValue("ns3::UniformRandomVariable[Min=595|Max=605]"), 
                                      "Y", StringValue("ns3::UniformRandomVariable[Min=65|Max=75]"), 
                                      "Z", StringValue("ns3::UniformRandomVariable[Min=1.2|Max=1.6]"));
    break;
  case 3://ななめ
    mobilityGnd.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                      "X", StringValue("ns3::UniformRandomVariable[Min=115|Max=125]"), 
                                      "Y", StringValue("ns3::UniformRandomVariable[Min=65|Max=75]"), 
                                      "Z", StringValue("ns3::UniformRandomVariable[Min=1.2|Max=1.6]"));
    break;
  default:
    break;
  }

  mobilityGnd.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityGnd.Install (gnds);

  std::cout <<"gnds　葦原小学校 ok\n";

  //地上ノードその２　ゆめあいセンター
  MobilityHelper mobilityGnd2;

  switch(G_TYPE){
  case 1://たて
    mobilityGnd2.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                       "X", StringValue("ns3::UniformRandomVariable[Min=595|Max=605]"), 
                                       "Y", StringValue("ns3::UniformRandomVariable[Min=425|Max=435]"), 
                                       "Z", StringValue("ns3::UniformRandomVariable[Min=1.6|Max=1.6]"));
    break;
  case 2://よこ(たてとおなじ)
    mobilityGnd2.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                       "X", StringValue("ns3::UniformRandomVariable[Min=595|Max=605]"), 
                                       "Y", StringValue("ns3::UniformRandomVariable[Min=425|Max=435]"), 
                                       "Z", StringValue("ns3::UniformRandomVariable[Min=1.6|Max=1.6]"));
    break;
  case 3://ななめ
    mobilityGnd2.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                       "X", StringValue("ns3::UniformRandomVariable[Min=1075|Max=1085]"), 
                                       "Y", StringValue("ns3::UniformRandomVariable[Min=425|Max=435]"), 
                                       "Z", StringValue("ns3::UniformRandomVariable[Min=1.6|Max=1.6]"));
    break;
  default:
    break;
  }

  mobilityGnd2.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityGnd2.Install (gnds2);

  std::cout <<"gnds2 ゆめあいセンター ok\n";

  //地上ノードその３　玉櫛公民館

  MobilityHelper mobilityGnd3;
  switch(G_TYPE){
  case 1://たて(よことおなじ)
    mobilityGnd3.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                       "X", StringValue("ns3::UniformRandomVariable[Min=115|Max=125]"), 
                                       "Y", StringValue("ns3::UniformRandomVariable[Min=245|Max=255]"), 
                                       "Z", StringValue("ns3::UniformRandomVariable[Min=1.6|Max=1.6]"));
    break;
  case 2://よこ
    mobilityGnd3.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                       "X", StringValue("ns3::UniformRandomVariable[Min=115|Max=125]"), 
                                       "Y", StringValue("ns3::UniformRandomVariable[Min=245|Max=255]"), 
                                       "Z", StringValue("ns3::UniformRandomVariable[Min=1.6|Max=1.6]"));
    break;
  case 3://ななめ
    mobilityGnd3.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                       "X", StringValue("ns3::UniformRandomVariable[Min=1075|Max=1085]"), 
                                       "Y", StringValue("ns3::UniformRandomVariable[Min=65|Max=75]"), 
                                       "Z", StringValue("ns3::UniformRandomVariable[Min=1.6|Max=1.6]"));
    break;
  default:
    break;
  }
 
  mobilityGnd3.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityGnd3.Install (gnds3);

  std::cout <<"gnds3　玉櫛公民館 ok\n";

  //地上ノードその４　南市民体育館

  MobilityHelper mobilityGnd4;
  switch(G_TYPE){
  case 1://たて(よことおなじ)
    mobilityGnd4.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                       "X", StringValue("ns3::UniformRandomVariable[Min=1075|Max=1085]"), 
                                       "Y", StringValue("ns3::UniformRandomVariable[Min=245|Max=255]"), 
                                       "Z", StringValue("ns3::UniformRandomVariable[Min=1.2|Max=1.6]"));
    break;
  case 2://よこ
    mobilityGnd4.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                       "X", StringValue("ns3::UniformRandomVariable[Min=1075|Max=1085]"), 
                                       "Y", StringValue("ns3::UniformRandomVariable[Min=245|Max=255]"), 
                                       "Z", StringValue("ns3::UniformRandomVariable[Min=1.2|Max=1.6]"));
    break;
  case 3://ななめ
    mobilityGnd4.SetPositionAllocator ("ns3::RandomBoxPositionAllocator",
                                       "X", StringValue("ns3::UniformRandomVariable[Min=115|Max=125]"), 
                                       "Y", StringValue("ns3::UniformRandomVariable[Min=425|Max=435]"), 
                                       "Z", StringValue("ns3::UniformRandomVariable[Min=1.2|Max=1.6]"));
    break;
  default:
    break;
  }
  mobilityGnd4.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityGnd4.Install (gnds4);

  std::cout <<"gnds4 南市民体育館 ok\n";

  // Create static grid(UAVノード)
  MobilityHelper mobilityUav;
  //Grid or Triangle or Honeycomb
 
  switch(TOPO_TYPE){
  case 1:
    mobilityUav.SetPositionAllocator ("ns3::GridPositionAllocator",
                                      "MinX", DoubleValue (0.0),
                                      "MinY", DoubleValue (0.0),
                                      "Z", DoubleValue(86.0),//UAVは上空86メートル
                                      "DeltaX", DoubleValue (step),
                                      "DeltaY", DoubleValue (step),
                                      "GridWidth", UintegerValue (12),
                                      "LayoutType", StringValue ("RowFirst"));
    break;
  case 2:
    mobilityUav.SetPositionAllocator ("ns3::TrianglePositionAllocator",
                                      "MinX", DoubleValue (0.0),
                                      "MinY", DoubleValue (0.0),
                                      "Z", DoubleValue(86.0),//UAVは上空86メートル
                                      "DeltaX", DoubleValue (step),
                                      "DeltaY", DoubleValue (step),
                                      "TriangleWidth", UintegerValue (12),
                                      "LayoutType", StringValue ("RowFirst"));
    break;
  case 3:
    mobilityUav.SetPositionAllocator ("ns3::HoneycombPositionAllocator",
                                      "MinX", DoubleValue (0.0),
                                      "MinY", DoubleValue (0.0),
                                      "Z", DoubleValue(86.0),//UAVは上空86メートル->86m
                                      "DeltaX", DoubleValue (step),
                                      "DeltaY", DoubleValue (step),
                                      "HoneycombWidth", UintegerValue (12),
                                      "LayoutType", StringValue ("RowFirst"));
    break;
  default:
    mobilityUav.SetPositionAllocator ("ns3::GridPositionAllocator",
                                      "MinX", DoubleValue (0.0),
                                      "MinY", DoubleValue (0.0),
                                      "Z", DoubleValue(86.0),//UAVは上空86メートル
                                      "DeltaX", DoubleValue (step),
                                      "DeltaY", DoubleValue (step),
                                      "GridWidth", UintegerValue (12),
                                      "LayoutType", StringValue ("RowFirst"));
    break;
  }

  mobilityUav.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityUav.Install (uavs);

  std::cout <<"uvas ok  mode:Grid\n";


  //追加するUAVノード

  MobilityHelper mobilityMove;

  Ptr<ListPositionAllocator> posListMove = CreateObject<ListPositionAllocator>();

  if(MOVE_FLAG==1){
    switch(G_TYPE){
    case 1://たて
      posListMove->Add(Vector(1050.0,350.0,86.0));//高度86m
      posListMove->Add(Vector(1050.0,450.0,86.0));
      posListMove->Add(Vector(1150.0,350.0,86.0));
      posListMove->Add(Vector(1150.0,450.0,86.0));
      posListMove->Add(Vector(1050.0,250.0,86.0));

      posListMove->Add(Vector(650.0 ,50.0 ,86.0));//
      posListMove->Add(Vector(750.0 ,50.0 ,86.0));
      posListMove->Add(Vector(450.0 ,150.0 ,86.0));
      posListMove->Add(Vector(550.0 ,150.0 ,86.0));
      posListMove->Add(Vector(650.0 ,150.0 ,86.0));
      posListMove->Add(Vector(750.0 ,150.0 ,86.0));
      posListMove->Add(Vector(450.0 ,250.0 ,86.0));
      posListMove->Add(Vector(550.0 ,250.0 ,86.0));
      posListMove->Add(Vector(650.0 ,250.0 ,86.0));
      posListMove->Add(Vector(750.0 ,250.0 ,86.0));
      posListMove->Add(Vector(450.0 ,350.0 ,86.0));//ここまで
      posListMove->Add(Vector(550.0 ,350.0,86.0));
      posListMove->Add(Vector(650.0,350.0,86.0));
      posListMove->Add(Vector(750.0,350.0,86.0));
      posListMove->Add(Vector(450.0,450.0,86.0));
      posListMove->Add(Vector(550.0,450.0,86.0));
      posListMove->Add(Vector(650.0,450.0,86.0));
      posListMove->Add(Vector(750.0,450.0,86.0));

      break;
    case 2://よこ
      posListMove->Add(Vector(1050.0 ,250.0 ,86.0));//  1
      posListMove->Add(Vector(1050.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(950.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(850.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(750.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(450.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(350.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(250.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(150.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(150.0 ,250.0 ,86.0));//

      posListMove->Add(Vector(50.0 ,250.0 ,86.0));//  11

      posListMove->Add(Vector(50.0 ,350.0 ,86.0));//
      posListMove->Add(Vector(50.0 ,450.0 ,86.0));//
      posListMove->Add(Vector(150.0 ,450.0 ,86.0));//
      posListMove->Add(Vector(250.0 ,450.0 ,86.0));//
      posListMove->Add(Vector(350.0 ,450.0 ,86.0));//
      posListMove->Add(Vector(450.0 ,450.0 ,86.0));//
      posListMove->Add(Vector(750.0 ,450.0 ,86.0));//
      posListMove->Add(Vector(850.0 ,450.0 ,86.0));//
      posListMove->Add(Vector(950.0 ,450.0 ,86.0));//
      posListMove->Add(Vector(1050.0 ,450.0 ,86.0));//  21

      posListMove->Add(Vector(1050.0 ,350.0 ,86.0));//
      posListMove->Add(Vector(950.0 ,250.0 ,86.0));//

      break;
    case 3://ななめ
      posListMove->Add(Vector(1050.0 ,350.0 ,86.0));//  1
      posListMove->Add(Vector(950.0 ,350.0 ,86.0));//
      posListMove->Add(Vector(850.0 ,350.0 ,86.0));//
      posListMove->Add(Vector(750.0 ,350.0 ,86.0));//
      posListMove->Add(Vector(650.0 ,350.0 ,86.0));//
      posListMove->Add(Vector(550.0 ,350.0 ,86.0));//
      posListMove->Add(Vector(450.0 ,350.0 ,86.0));//
      posListMove->Add(Vector(350.0 ,350.0 ,86.0));// 8

      posListMove->Add(Vector(1050.0 ,250.0 ,86.0));//
      posListMove->Add(Vector(950.0 ,250.0 ,86.0));//
      posListMove->Add(Vector(950.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(850.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(750.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(650.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(550.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(450.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(350.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(250.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(150.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(150.0 ,250.0 ,86.0));//  20
      posListMove->Add(Vector(50.0 ,250.0 ,86.0));//
      posListMove->Add(Vector(50.0 ,150.0 ,86.0));//
      posListMove->Add(Vector(50.0 ,50.0 ,86.0));//


      break;
    default:
      posListMove->Add(Vector(2000.0,2000.0,86.0));//
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));//
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));
      posListMove->Add(Vector(2000.0,2000.0,86.0));

      break;
    }
    mobilityMove.SetPositionAllocator(posListMove);
  }else{
  
    mobilityMove.SetPositionAllocator ("ns3::GridPositionAllocator",
                                       "MinX", DoubleValue (183.33),
                                       "MinY", DoubleValue (125.0),
                                       "Z", DoubleValue(86.0),
                                       "DeltaX", DoubleValue (128.33),
                                       "DeltaY", DoubleValue (125.0),
                                       "GridWidth", UintegerValue (6),
                                       "LayoutType", StringValue ("RowFirst"));
  }

  mobilityMove.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobilityMove.Install (move1);


}

//ネットワークデバイス作成関数
//作成 2018/10/30
void
FanetSimulation::CreateDevices ()
{
  //WifiMacHelper wifiMac;
  wifiMac.SetType ("ns3::AdhocWifiMac");
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
  wifiPhy.SetChannel (wifiChannel.Create ());

  WifiHelper wifi;
  
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate6Mbps"), "RtsCtsThreshold", UintegerValue (0));
  devices = wifi.Install (wifiPhy, wifiMac, nodes); 

  if (pcap)
    {
      wifiPhy.EnablePcapAll (std::string ("aodv-job"));
    }
  AsciiTraceHelper ascii;
  wifiPhy.EnableAsciiAll (ascii.CreateFileStream ("fanet.tr"));
}

//インターネット設定関数
//作成 2018/10/30
void
FanetSimulation::InstallInternetStack ()
{
  AodvHelper aodv;
  // you can configure AODV attributes here using aodv.Set(name, value)
  InternetStackHelper stack;
  stack.SetRoutingHelper (aodv); // has effect on the next Install ()
  stack.Install (nodes);
  Ipv4AddressHelper address;
  address.SetBase ("10.0.0.0", "255.0.0.0");
  interfaces = address.Assign (devices);

  if (printRoutes)
    {
      Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> ("aodv.routes", std::ios::out);
      aodv.PrintRoutingTableAllAt (Seconds (8), routingStream);
    }
}

//アプリケーションインストール関数
//作成 2018/10/30
void
FanetSimulation::InstallApplications ()
{
  /* V4PingHelper ping (interfaces.GetAddress (29));//original: size - 1
     ping.SetAttribute ("Verbose", BooleanValue (true));

     ApplicationContainer p = ping.Install (nodes.Get (0));
     p.Start (Seconds (0));
     p.Stop (Seconds (totalTime) - Seconds (0.001));

     // move node away
     Ptr<Node> node = nodes.Get (size/2);
     Ptr<MobilityModel> mob = node->GetObject<MobilityModel> ();
     Simulator::Schedule (Seconds (totalTime/3), &MobilityModel::SetPosition, mob, Vector (1e5, 1e5, 1e5));

  */
  ///*

  //unsigned int sum_gnd = size + size2;
  unsigned int index_server = uav;
  unsigned int index_client = uav+size;
  bool first_check = true;
  if(first_check){
    first_check=false;
    std::cout<<"index_server:"<<index_server<<"　index_client:"<<index_client<<"\n";
  }

  uint32_t app_num=1;
  uint32_t addr_srv;
  uint32_t addr_clt;

  //1個目
  switch(G_TYPE){
  case 1://たて
    addr_srv=72;
    addr_clt=78;
    break;
  case 2://よこ
    addr_srv=84;
    addr_clt=90;
    break;
  case 3://右肩さがりななめ
    addr_srv=72;
    addr_clt=78;
    break;
  default:
    break;
  }

  std::cout<< app_num++ <<"番目のアプリケーション\n";
  std::cout<<"addr_srv:"<< addr_srv <<"  ";
  std::cout<<"addr_clt:"<< addr_clt <<"\n";

  serverAddress = Address(interfaces.GetAddress(/*index_server*/addr_srv));//もともと0
  //std::cout<<"fanet.cc アプリケーションテスト ok\n";
  uint16_t port = 4000;
  UdpServerHelper server(port);
  ApplicationContainer apps = server.Install(nodes.Get(/*index_server++*/addr_srv));//もともと0
  apps.Start(Seconds(5.0));
  apps.Stop(Seconds(80.0));

  uint32_t MaxPacketSize;
  Time interPacketInterval;//default 0.05  voip:0.02

  MaxPacketSize = 1024;
  interPacketInterval = Seconds(0.02);//default 0.05  voip:0.02

  uint32_t maxPacketCount =10000 ;//default 3200
  UdpClientHelper client(serverAddress,port);
  client.SetAttribute("MaxPackets",UintegerValue(maxPacketCount));
  client.SetAttribute("Interval",TimeValue(interPacketInterval));
  client.SetAttribute("PacketSize",UintegerValue(MaxPacketSize));
  
  apps = client.Install(nodes.Get(/*index_client++*/addr_clt));//29 or 51
  //std::cout << " lost packet "<<server.GetLost()<< "\n";
  apps.Start(Seconds(5.0));
  apps.Stop(Seconds(70.0));

  //2個目
  switch(G_TYPE){
  case 1://たて
    addr_srv=73;
    addr_clt=79;
    break;
  case 2://よこ
    addr_srv=85;
    addr_clt=91;
    break;
  case 3://右肩さがりななめ
    addr_srv=73;
    addr_clt=79;
    break;
  default:
    break;
  }
  std::cout<< app_num++ <<"番目のアプリケーション\n";
  std::cout<<"addr_srv:"<< addr_srv <<"  ";
  std::cout<<"addr_clt:"<< addr_clt <<"\n";

  //interPacketInterval = Seconds(0.02);//default 0.05  voip:0.02

  serverAddress = Address(interfaces.GetAddress(addr_srv));//もともと0
  //std::cout<<"fanet.cc アプリケーションテスト ok\n";
  UdpServerHelper server2(port);
  ApplicationContainer apps2 = server2.Install(nodes.Get(addr_srv));//もともと0
  apps2.Start(Seconds(5.0));
  apps2.Stop(Seconds(70.0));

  MaxPacketSize = 1024;
  interPacketInterval = Seconds(0.02);//default 0.05  voip:0.02

  UdpClientHelper client2(serverAddress,port);
  client2.SetAttribute("MaxPackets",UintegerValue(maxPacketCount));
  client2.SetAttribute("Interval",TimeValue(interPacketInterval));
  client2.SetAttribute("PacketSize",UintegerValue(MaxPacketSize));
  
  apps2 = client2.Install(nodes.Get(addr_clt));//29 or 51
  //std::cout << " lost packet "<<server.GetLost()<< "\n";
  apps2.Start(Seconds(10.0));
  apps2.Stop(Seconds(70.0));

  //３個目
  switch(G_TYPE){
  case 1://たて
    addr_srv=74;
    addr_clt=80;
    break;
  case 2://よこ
    addr_srv=86;
    addr_clt=92;
    break;
  case 3://右肩さがりななめ
    addr_srv=74;
    addr_clt=80;
    break;
  default:
    break;
  }
  std::cout<< app_num++ <<"番目のアプリケーション\n";
  std::cout<<"addr_srv:"<< addr_srv <<"  ";
  std::cout<<"addr_clt:"<< addr_clt <<"\n";

  serverAddress = Address(interfaces.GetAddress(addr_srv));//もともと0
  //std::cout<<"fanet.cc アプリケーションテスト ok\n";
  UdpServerHelper server3(port);
  ApplicationContainer apps3 = server3.Install(nodes.Get(addr_srv));//もともと0
  apps3.Start(Seconds(5.0));
  apps3.Stop(Seconds(80.0));

  MaxPacketSize = 1024;
  interPacketInterval = Seconds(0.02);//default 0.05  voip:0.02

  UdpClientHelper client3(serverAddress,port);
  client3.SetAttribute("MaxPackets",UintegerValue(maxPacketCount));
  client3.SetAttribute("Interval",TimeValue(interPacketInterval));
  client3.SetAttribute("PacketSize",UintegerValue(MaxPacketSize));
  
  apps3 = client3.Install(nodes.Get(addr_clt));//29 or 51
  //std::cout << " lost packet "<<server.GetLost()<< "\n";
  apps3.Start(Seconds(15.0));
  apps3.Stop(Seconds(70.0));

  //4個目
  switch(G_TYPE){
  case 1://たて
    addr_srv=75;
    addr_clt=81;
    break;
  case 2://よこ
    addr_srv=87;
    addr_clt=93;
    break;
  case 3://右肩さがりななめ
    addr_srv=75;
    addr_clt=81;
    break;
  default:
    break;
  }
  std::cout<< app_num++ <<"番目のアプリケーション\n";
  std::cout<<"addr_srv:"<< addr_srv <<"  ";
  std::cout<<"addr_clt:"<< addr_clt <<"\n";

  serverAddress = Address(interfaces.GetAddress(addr_srv));//もともと0
  //std::cout<<"fanet.cc アプリケーションテスト ok\n";
  UdpServerHelper server4(port);
  ApplicationContainer apps4 = server4.Install(nodes.Get(addr_srv));//もともと0
  apps4.Start(Seconds(5.0));
  apps4.Stop(Seconds(80.0));

  MaxPacketSize = 512;
  interPacketInterval = Seconds(0.02);//default 0.05  voip:0.02

  UdpClientHelper client4(serverAddress,port);
  client4.SetAttribute("MaxPackets",UintegerValue(maxPacketCount));
  client4.SetAttribute("Interval",TimeValue(interPacketInterval));
  client4.SetAttribute("PacketSize",UintegerValue(MaxPacketSize));
  
  apps4 = client4.Install(nodes.Get(addr_clt));//29 or 51
  //std::cout << " lost packet "<<server.GetLost()<< "\n";
  apps4.Start(Seconds(20.0));
  apps4.Stop(Seconds(70.0));

  //5個目
  switch(G_TYPE){
  case 1://たて
    addr_srv=76;
    addr_clt=82;
    break;
  case 2://よこ
    addr_srv=88;
    addr_clt=94;
    break;
  case 3://右肩さがりななめ
    addr_srv=76;
    addr_clt=82;
    break;
  default:
    break;
  }
  std::cout<< app_num++ <<"番目のアプリケーション\n";
  std::cout<<"addr_srv:"<< addr_srv <<"  ";
  std::cout<<"addr_clt:"<< addr_clt <<"\n";

  serverAddress = Address(interfaces.GetAddress(addr_srv));//もともと0
  //std::cout<<"fanet.cc アプリケーションテスト ok\n";
  UdpServerHelper server5(port);
  ApplicationContainer apps5 = server5.Install(nodes.Get(addr_srv));//もともと0
  apps5.Start(Seconds(5.0));
  apps5.Stop(Seconds(80.0));

  MaxPacketSize = 1024;
  interPacketInterval = Seconds(0.02);//default 0.05  voip:0.02

  UdpClientHelper client5(serverAddress,port);
  client5.SetAttribute("MaxPackets",UintegerValue(maxPacketCount));
  client5.SetAttribute("Interval",TimeValue(interPacketInterval));
  client5.SetAttribute("PacketSize",UintegerValue(MaxPacketSize));
  
  apps5 = client5.Install(nodes.Get(addr_clt));//29 or 51
  //std::cout << " lost packet "<<server.GetLost()<< "\n";
  apps5.Start(Seconds(25.0));
  apps5.Stop(Seconds(70.0));

  //6個目
  switch(G_TYPE){
  case 1://たて
    addr_srv=77;
    addr_clt=83;
    break;
  case 2://よこ
    addr_srv=89;
    addr_clt=95;
    break;
  case 3://右肩さがりななめ
    addr_srv=77;
    addr_clt=83;
    break;
  default:
    break;
  }
  std::cout<< app_num++ <<"番目のアプリケーション\n";
  std::cout<<"addr_srv:"<< addr_srv <<"  ";
  std::cout<<"addr_clt:"<< addr_clt <<"\n";

  serverAddress = Address(interfaces.GetAddress(addr_srv));//もともと0
  //std::cout<<"fanet.cc アプリケーションテスト ok\n";
  UdpServerHelper server6(port);
  ApplicationContainer apps6 = server6.Install(nodes.Get(addr_srv));//もともと0
  apps6.Start(Seconds(5.0));
  apps6.Stop(Seconds(80.0));

  MaxPacketSize = 512;
  interPacketInterval = Seconds(0.02);//default 0.05  voip:0.02

  UdpClientHelper client6(serverAddress,port);
  client6.SetAttribute("MaxPackets",UintegerValue(maxPacketCount));
  client6.SetAttribute("Interval",TimeValue(interPacketInterval));
  client6.SetAttribute("PacketSize",UintegerValue(MaxPacketSize));
  
  apps6 = client6.Install(nodes.Get(addr_clt));//29 or 51
  //std::cout << " lost packet "<<server.GetLost()<< "\n";
  apps6.Start(Seconds(30.0));
  apps6.Stop(Seconds(70.0));
  /**/


  //*/
}

int hoge=0;
int num_RxBegin=0;
int num_RxEnd=0;
int num_TxBegin=0;
int num_TxEnd=0;
int dekai=0;

int num_rx=0;


//パケットドロップのトレース
long m_drop_L1=0;
long m_drop_L2=0;
long m_drop_L3=0;


int fileIndexInt=0;

void scheduleFunc()
{
  std::cout<<"予約テスト(fanet.cc) Time:  "<< Simulator::Now().GetSeconds()<<" s\n";
}

/************************************************** 
 *
 *
 *
 *
 *
 *
 * 2020/01/10 ここから下に混雑度取得のコールバックを作成
 *
 *
 *
 *
 ***************************************************/
//混雑度取得
void phyCongestion(
                   std::string context,
                   Ptr<const Packet> packet
                   ){
  //printf("*");
  //std::cout<<"受\n";
  //PhyTxEnd  node /NodeList/10/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd  Time:11.2153ちいさい

  int event_node;

  char tmp[100]={};
  strcpy(tmp,context.c_str());
  char *ptr=NULL;
  ptr=strtok(tmp,"/");
  if(ptr!=NULL){
    ptr=strtok(NULL,"/");
    if(ptr!=NULL){
      //printf("## r1 Node&%d&",atoi(ptr));
      event_node = atoi(ptr);
                        
    }
  }

  cong[event_node] += packet->GetSize();

}

//重さ取得
void L3weight(
              std::string context,
              Ptr<const Packet> p,
              Ptr<Ipv4> ipv4,
              uint32_t interface
              ){

  Ptr<Packet> p_cp;
  p_cp = p->Copy();

  //header 解析テスト
  PacketMetadata::ItemIterator it = p_cp->BeginItem();
  PacketMetadata::Item item;

  int flag_seqts=0;

  while(it.HasNext()){
    item = it.Next();
    if(item.type == PacketMetadata::Item::HEADER){
      if(item.tid.GetName() == "ns3::SeqTsHeader"){
        NS_LOG_INFO("SeqTs header found.");
        //std::cout<<"SeqTs header found.\n";
        flag_seqts=1;
        break;
      }   
    }
  }

  if(flag_seqts==0)
    return;

  //
  int event_node;

  char tmp[100]={};
  strcpy(tmp,context.c_str());
  char *ptr=NULL;
  ptr=strtok(tmp,"/");
  if(ptr!=NULL){
    ptr=strtok(NULL,"/");
    if(ptr!=NULL){
      //printf("## r1 Node&%d&",atoi(ptr));
      event_node = atoi(ptr);
                        
    }
  }

  //TypeHeader tHeader (AODVTYPE_RREQ);
  Ipv4Header ipv4Header;
  p_cp->RemoveHeader(ipv4Header);
  //p->AddHeader(ipv4Header);

  Ipv4Address ipv4_dest = ipv4Header.GetDestination ();
  uint32_t ipv4_10 = ipv4_dest.Get();
  int last_num_ipv4_10 = (ipv4_10 >> 0) & 0xff;
  
  //IPアドレスをノードIDに変換するには-1が必要
  last_num_ipv4_10 = last_num_ipv4_10 - 1;

  int dest_ID = dstIndex(last_num_ipv4_10);

  if(last_num_ipv4_10 == 255){
    return;
  }


  //  std::cout<<"p->size:"<<p->GetSize()<<"\n";

  if(p->GetSize() >= 500)
    weight[event_node][dest_ID] += p->GetSize();

}


//混雑度・フローの重さリセット
void reset()
{
  for(int i=0;i<NODE;i++){
    cong[i]=0.0;
    for(int j=0;j<6;j++){
      weight[i][j]=0.0;
    }
  }
}

int no=1;

//5秒毎に移動許可レベルを表示
void moveLevel(){

  int node_ID_uav=0;
  int move_level[6][12];
  int move_judge[6][12];
  //(1) 各ノードの混雑度だけで判定
  for(int i=0;i<6;i++){
    for(int j=0;j<12;j++){
      move_judge[i][j] = 0;
      double weight_on_uav = 0;
      for(int t=0;t<6;t++){
        weight_on_uav += weight[node_ID_uav][t];
      }
      if((weight_on_uav * 8.0) >= (double)((1024*1024)*3.5) )
        move_level[i][j] = 10;
      else if((weight_on_uav * 8) >= 1000000 )
        move_level[i][j] = 5;
      else
        move_level[i][j] = 0;
      node_ID_uav++;
    }
  }

  int level_flag = 0;
  //(2) 混雑度１０のノードから周囲に向かってレベル設定
  for(int i=0;i<6;i++){
    for(int j=0;j<12;j++){
      if(move_level[i][j] >= 10){
        level_flag = 1;
        int s;
        int t;
        for(s=i-2;s<=i+2;s++){
          for(t=j-2;t<=j+2;t++){
            if(s >=0 && s<=5 && t>=0 && t<= 11){
              int level = abs(i-s)+abs(j-t);
              if(level==1)//1ホップ
                move_judge[s][t] += move_level[s][t] + move_level[i][j] * 0.5;
              else if(level==2)//２ホップ
                move_judge[s][t] += move_level[s][t] + move_level[i][j] * 0.25;
              if(move_judge[s][t] >= 100)
                move_judge[s][t] = 99;
                
            }
          }
        }
      }
    }
  }

  if(level_flag == 1){
    std::cout<<"ーーーーーーー移動許可レベル at " << no << "  ーーーーーーーー\n";
    for(int i=0;i<6;i++){
      for(int j=0;j<12;j++){
        if( move_level[i][j] >= 10)
          printf("\x1b[36m");
        else if(move_judge[i][j] >= 5)
          printf("\x1b[32m");
        printf(" %2d ",move_judge[i][j]);
        printf("\x1b[39m");
      }
      puts("");
    }
    std::cout<<"ーーーーーーーーーーーーーーーーーーーーーーーーーー\n";
  }
}


void outputCongestion(){

  double cong_level = (double)cong[34] / (double)(1024*1024/8);

  //std::cout<<"cong_level: " << cong_level << "\n";

  if(no % 5 == 0)
    printf("cong_level(%2d): %lf\n",no,cong_level);


  //  int id=100;// イベント発生したノードID
  //char tmp[100]={};
 
  FILE *fp;
  char filename[BUF]={'\0'};
  //  char str[BUF]={'\0'};




  //ネーミングのベース部分
  strcpy(filename,"./src/aodv/model/congestion.txt");

      
  //ファイルポインタを設定
  fp = fopen(filename,"w");
  if(fp==NULL){
    printf("miss %s\n",filename);
    return;
  }

  /*
    char all_weight[BUF]={'\0'};
    for(int j=0;j<6;j++){
    char str_weight[BUF]={'\0'};
    sprintf(str_weight, "%lf",weight[i][j]);
    strcat(all_weight,str_weight);
    if(j<5)
    strcat(all_weight," ");  
    }
  */

  for(int i=0;i<NODE;i++){
    char all_weight[BUF]={'\0'};
    char str_cong[BUF]={'\0'};
    sprintf(str_cong, "%.2lf",cong[i]);

    for(int j=0;j<6;j++){
      char str_weight[BUF]={'\0'};
      sprintf(str_weight, "%0.2lf",weight[i][j]);
      strcat(all_weight,str_weight);
      if(j<5)
        strcat(all_weight," ");  
    }
    //    fprintf(fp,"nodeID 混雑度 フロー1 フロー2 フロー3 フロー4 フロー5 フロー6",str);
    fprintf(fp,"Node%03d %s %s\n",i,str_cong,all_weight);
  }

  fclose(fp);
 
  //if(no % 5 == 0)
  moveLevel();

  reset();
  no++;
}

void phyRxDrop(
               std::string context,
               Ptr<const Packet> packet
               ){
  
  m_drop_L1++;
   
}


int 
main (int argc, char *argv[])
{
  ns3::PacketMetadata::Enable();
  FanetSimulation fanet;
  if (!fanet.Configure (argc, argv))
    NS_FATAL_ERROR ("Configuration failed. Aborted.");

  fanet.CreateNodes ();
  fanet.CreateDevices ();
  fanet.InstallInternetStack ();
  fanet.InstallApplications ();

  std::cout << "Starting simulation for " << fanet.totalTime << " s ...\n";

  //トレースファイル作成
  //AsciiTraceHelper ascii;
  //devices.EnableAsciiAll(ascii.CreateFileStream("fanet.tr"));

  //遅延計測フロウモニタflow-monitorテスト  
  Ptr<FlowMonitor> flowMonitor;
  FlowMonitorHelper flowHelper;
  flowMonitor = flowHelper.InstallAll();
  flowMonitor -> SerializeToXmlFile("flowmon.xml",true,true);

  Simulator::Stop (Seconds (fanet.totalTime));

  //トレースコールバック関数を設定
  Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyTxEnd",MakeCallback(&phyCongestion));
  Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyRxEnd",MakeCallback(&phyCongestion));
  Config::Connect("/NodeList/*/$ns3::Ipv4L3Protocol/Rx",MakeCallback(&L3weight));
  Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyRxDrop",MakeCallback(&phyRxDrop));

  int t;
  for(t=1;t<fanet.totalTime;t++){
    Simulator::Schedule (Seconds (t), &outputCongestion);
  }

  Simulator::Run ();

  //遅延計測フロウモニタflow-monitorテスト 
  //print per flow statistics ...(・o・?)

  flowMonitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowHelper.GetClassifier ());
  FlowMonitor::FlowStatsContainer stats = flowMonitor->GetFlowStats ();

  Time allSum;
  double sendSum=0;
  double recvSum=0;

  double throughput = 0;
  double offered = 0;
  int m_flow=0;
  int m_udp=0;

  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {
      Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
      if(i->second.txPackets >= 1000){
        std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
        std::cout << "  Tx Packets: " << i->second.txPackets << "\n";
        //std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
        //std::cout << "  TxOffered:  " << i->second.txBytes * 8.0 / (fanetDumbbell.totalTime) / 1000 / 1000  << " Mbps\n";
        std::cout << "  Rx Packets: " << i->second.rxPackets << "\n";
        //std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
        std::cout << "  delaySum(nsって書いてるけどマイクロ秒):   " << i->second.delaySum/(1000*1000) << "\n";
        //std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / (fanet.totalTime) / 1000 / 1000  << " Mbps\n";
        sendSum += i->second.txPackets;
        recvSum += i->second.rxPackets;
        allSum += i->second.delaySum;
        std::cout << "\n";
        m_udp++;
        m_flow++;
        throughput += i->second.rxBytes * 8.0 / (fanet.totalTime) / 1000 / 1000;
        offered += i->second.txBytes * 8.0 / (fanet.totalTime) / 1000 / 1000;
      }


    }

  Simulator::Destroy ();
  fanet.Report (std::cout);

  std::cout<<"||| -------------------------\n";
  std::cout<<"\n||| 全フロー合計遅延時間 : "<< allSum <<"\n";
  std::cout<<"\n||| 全フロー平均遅延時間 : "<< allSum / m_udp <<"\n";
  std::cout<<"||| 送信パケット合計: "<<sendSum<<"\n";
  std::cout<<"||| 受信パケット合計: "<<recvSum<<"\n";
  if(sendSum!=0)
    std::cout<<"||| パケット到達率: "<< double(recvSum / sendSum) * 100.0<<"\n";
  std::cout<<"||| Avg of TxOffered:"<< offered / m_flow <<"Mbps\n";
  std::cout<<"||| Avg of Throughput:"<< throughput / m_flow <<"mbps\n";
  std::cout<<"||| -------------------------\n";
  std::cout<<"||| -------------------------\n";
  std::cout<<"||| m_drop_L1:"<< m_drop_L1 <<"\n";
  std::cout<<"||| m_drop_L2:"<< m_drop_L2 <<"\n";
  std::cout<<"||| m_drop_L3:"<< m_drop_L3 <<"\n";
  std::cout<<"||| -------------------------\n";
  printf("\n||| 自作シミュレーション実行成功\n");
  return 0;
}
