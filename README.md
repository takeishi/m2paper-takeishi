
2020/02/04 修士論文研究 README

1 概要 -------------------------------------------- 

ネットワークシステム研究室M2の武石が2018年4月から2020年3月まで行った
大学院での研究を通じて作成・修正したプログラムの実行方法をまとめた。
また、対外発表についての情報をまとめた。



2 実行環境 -------------------------------------------- 

2.1 OS 

Ubuntu 14.04 LTS
(VMware Workstation 12 Playerを使用し仮想マシンとして構築)


2.2 シミュレータ 

ns-3.26



3 実行方法 --------------------------------------------

3.0 ns3のインストール
README.mdに書くとGitLab上で改行がうまく表示されず読みにくいので
別ファイル「nsインストール方法.txt」に記載した。

3.1 ソースコードの入れ替え

以下のファイルについて、既存のものを武石のものに置き換える

・/home/ns3/ns-3.26/bake/source/ns-3.26/src/netanim/examples/fanet.cc

・/home/ns3/ns-3.26/bake/source/ns-3.26/src/netanim/examples/wscript

・/home/ns3/ns-3.26/bake/source/ns-3.26/src/aodv/model/aodv-routing-protocol.cc

・/home/ns3/ns-3.26/bake/source/ns-3.26/src/aodv/model/aodv-routing-protocol.h

・/home/ns3/ns-3.26/bake/source/ns-3.26/src/applications/model/udp-client.cc

・/home/ns3/ns-3.26/bake/source/ns-3.26/src/applications/model/udp-client.h

・/home/ns3/ns-3.26/bake/source/ns-3.26/src/applications/model/udp-server.cc

・/home/ns3/ns-3.26/bake/source/ns-3.26/src/applications/model/udp-server.h



3.2 ソースコードの設定

3.1のソースコードのうち、以下のものはソースコードの一部記述を変えてシミュレーションを変更する。

「fanet.cc」

・７２行目　G_TYPEの値を設定して地上ノードの配置位置とアプリケーション通信の方向を決める

1： 360メートル離れた地上ノード間で、FANETに対して縦方向に通信

2： 960メートル離れた地上ノード間で、FANETに対して横方向に通信

3：1025メートル離れた地上ノード間で、FANETに対してななめ方向に通信

・７６行目　 TOPO_TYPEの値を設定してFANETのトポロジを変更
＃最終的には使用していない、Gridで固定

・８０行目　MOVE_FLAGの値を設定してUAVノードの追加配置をするか決定する


「aodv-routing-protocol.cc」

・７４行目　normalの値を設定してホップ数のルールを決める

0:なにもしない（従来のAODV）

1:追加ホップ数を与える（UAVノードの追加配置なし）

2:追加ホップ数を与える（UAVノードの追加配置あり）

・７７行目　groundの値を変えてfanet.ccで設定した通信の方向に合わせる


3.3 シミュレーション実行

/home/ns3/ns-3.26/bake/source/ns-3.26/　で以下のようにコマンドを実行

./waf --run fanet

端末上に結果が表示されていく。

4 研究会発表について --------------------------------------------

2019年8月27日から同28日にかけて開催された「電子情報通信学会　コミュニケーションクオリティ研究会」にて
研究発表を行った。この研究会の開催プログラムにアクセスするURLを以下に記載する。

https://www.ieice.org/ken/program/index.php?tgs_regid=5715a7d8df712afd0c3d2250841b80184aa8ca2ca38d4801992c276fb8e1afab

また、当該発表についての情報にアクセスするURLを以下に記載する。

https://www.ieice.org/ken/paper/20190828N1Pm/

